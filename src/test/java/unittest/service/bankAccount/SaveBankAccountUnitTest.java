package unittest.service.bankAccount;

import kiwi.itech.repository.IBankAccountDao;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import kiwi.itech.model.entities.BankAccountOwner;
import kiwi.itech.kiwibackend.dtos.BankAccountDto;
import kiwi.itech.service.impl.BankAccountService;

import java.util.UUID;

@RunWith(MockitoJUnitRunner.class)
public class SaveBankAccountUnitTest {

	
	@Mock(name = "BankAccountDao")
    private IBankAccountDao bankAccountDao;
    @InjectMocks
    @Spy
    private BankAccountService bankAccountService;

    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }
    
    @Test
    public void whenIbanAlreadyExist() {
        //given

    	BankAccountDto accountDto = new BankAccountDto()
                .setAccountOwner(new BankAccountOwner())
                .setBic("DEUTDEDBHAM")
                .setIban("DE682007002400428321000")
                .setBankName("Deutsche Bank")
                .setId(UUID.randomUUID().toString().substring(0,6));
    	
    	Mockito.doNothing().when(bankAccountService).validationBankAccount(accountDto);
    	
    	//Mockito.when(bankAccountDao.save(accountDto.toPojo())).thenReturn(accountDto.toPojo());
    	
        //when
    	
        BankAccountDto result = bankAccountService.saveBankAccount(accountDto);
        
        //Then
        
        Assert.assertNull(result);
    }
}
