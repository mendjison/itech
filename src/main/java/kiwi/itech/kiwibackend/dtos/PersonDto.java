package kiwi.itech.kiwibackend.dtos;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import kiwi.itech.model.entities.PhoneNumber;
import kiwi.itech.model.entities.enums.GenderType;

public class PersonDto {

	private String personId;
	private String firstname;
	private String lastname;
	private String birthName;
	private String email;
	private GenderType gender;
	//@JsonFormat(pattern = "yyyy-MM-dd")
	@JsonProperty("dateOfBirth")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dateOfBirth;
	private String taxNo;
	private String nationality;
	private String localOfBirth;
	private String additionAddress;
	private AddressDto address;
	private List<PhoneNumber> phones;
	
	public PersonDto() {}

	public String getPersonId() {
		return personId;
	}

	public PersonDto setPersonId(String personId) {
		this.personId = personId;
		return this;
	}

	public String getFirstname() {
		return firstname;
	}

	public PersonDto setFirstname(String firstname) {
		this.firstname = firstname;
		return this;
	}

	public String getLastname() {
		return lastname;
	}

	public PersonDto setLastname(String lastname) {
		this.lastname = lastname;
		return this;
	}

	public String getBirthName() {
		return birthName;
	}

	public PersonDto setBirthName(String birthName) {
		this.birthName = birthName;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public PersonDto setEmail(String email) {
		this.email = email;
		return this;
	}

	public GenderType getGender() {
		return gender;
	}

	public PersonDto setGender(GenderType gender) {
		this.gender = gender;
		return this;
	}

	public LocalDate getDateOfBirth() {
		if(this.dateOfBirth == null)
			this.dateOfBirth = LocalDate.now();
		return dateOfBirth;
	}

	public PersonDto setDateOfBirth(LocalDate dateOfBirth) {
		if(this.dateOfBirth == null)
			this.dateOfBirth = LocalDate.now();
		this.dateOfBirth = dateOfBirth;
		return this;
	}

	public String getTaxNo() {
		return taxNo;
	}

	public PersonDto setTaxNo(String taxNo) {
		this.taxNo = taxNo;
		return this;
	}

	public String getNationality() {
		return nationality;
	}

	public PersonDto setNationality(String nationality) {
		this.nationality = nationality;
		return this;
	}

	public String getLocalOfBirth() {
		return localOfBirth;
	}

	public PersonDto setLocalOfBirth(String localOfBirth) {
		this.localOfBirth = localOfBirth;
		return this;
	}

	public String getAdditionAddress() {
		return additionAddress;
	}

	public PersonDto setAdditionAddress(String additionAddress) {
		this.additionAddress = additionAddress;
		return this;
	}

	public AddressDto getAddress() {
		return address;
	}

	public PersonDto setAddress(AddressDto address) {
		this.address = address;
		return this;
	}

	public List<PhoneNumber> getPhones() {
		if(this.phones == null)
			this.phones = new ArrayList<PhoneNumber>();
		return phones;
	}

	public PersonDto setPhones(List<PhoneNumber> phones) {
		this.phones = phones;
		return this;
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", PersonDto.class.getSimpleName() + "[", "]")
				.add("personId='" + personId + "'")
				.add("firstname='" + firstname + "'")
				.add("lastname='" + lastname + "'")
				.add("birthName='" + birthName + "'")
				.add("email='" + email + "'")
				.add("gender=" + gender)
				.add("dateOfBirth=" + dateOfBirth)
				.add("taxNo='" + taxNo + "'")
				.add("nationality='" + nationality + "'")
				.add("localOfBirth='" + localOfBirth + "'")
				.add("additionAddress='" + additionAddress + "'")
				.add("address=" + address)
				.add("phones=" + phones)
				.toString();
	}
}
