package kiwi.itech.model.entities.enums;

public enum PersonType {

	APPLICANT, CHILD, PARTNER, CLERK
}
