package kiwi.itech.repository;

import kiwi.itech.model.entities.Address;
import kiwi.itech.repository.generic.JpaRepository;

import javax.ejb.Local;

@Local
public interface IAddressDao extends JpaRepository<Address, String> {

    Address findAddress(String street, String houseNo, int postcode, String country);
}
