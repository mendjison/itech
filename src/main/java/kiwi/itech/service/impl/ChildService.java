package kiwi.itech.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import kiwi.itech.model.entities.Child;
import kiwi.itech.model.helper.exception.AlreadyExistException;
import kiwi.itech.model.helper.exception.ExceptionMessage;
import kiwi.itech.model.helper.exception.NotNullException;
import kiwi.itech.repository.IChildDao;
import kiwi.itech.service.IAddressService;
import kiwi.itech.service.IChildService;
import kiwi.itech.kiwibackend.dtos.ChildDto;

@Stateless(name = "ChildService")
public class ChildService implements IChildService {

	@EJB(name = "ChildDao")
	private IChildDao childDao;
	@EJB(name = "AddressService")
	private IAddressService addressService;


	public ChildService() { }

	@Override
	public ChildDto saveChild(ChildDto childDto) {

		validateSave(childDto);
		addressService.findOrSaveAddress(childDto);

		childDto.setPersonId(UUID.randomUUID().toString().substring(0,6));
		return  childDao.save(childDto.toPojo()).toDto();
	}

	@Override
	public ChildDto updateChild(String childId, ChildDto childDto) {

		validateUpdate(childId, childDto);

		Optional<Child> childOptional = childDao.findById(childId);
		if(childOptional.isPresent()) {
			addressService.findOrSaveAddress(childDto);
			return  childDao.update(childDto.setPersonId(childOptional.get().getPersonId()).toPojo()).toDto();
		}
		else {
			return saveChild(childDto);
		}
	}

	@Override
	public ChildDto findChild(String childId) {
		Child child = childDao.findById(childId).orElse(null);
		if (child == null)
			return null;

		return child.toDto();
	}

	@Override
	public List<ChildDto> findChilds() {

		return childDao.findAll().stream()
				.map((Child child) -> child.toDto())
				.collect(Collectors.toList());
	}

	@Override
	public boolean deleteChild(String childId) {
		Child child = childDao.findById(childId).orElse(null);
		if (child == null)
			return false;

		return childDao.delete(child);
	}

	@Override
	public ChildDto findByTaxNo(String taxNo) {
		Child child = childDao.findByTaxNo(taxNo).orElse(null);
		if (child == null)
			return null;

		return child.toDto();
	}

	private void validateSave(ChildDto childDto) {

		if (childDto == null)
			throw new NotNullException(ExceptionMessage.CHILD_MUST_NOT_NULL);

		if (childDto.getTaxNo() == null || childDto.getTaxNo().equals(""))
			throw new NotNullException(ExceptionMessage.TAXNO_MUST_NOT_NULL);

		if (findByTaxNo(childDto.getTaxNo()) != null)
			throw new AlreadyExistException(ExceptionMessage.TAXNO_ALREADY_EXISTS);
	}

	private void validateUpdate(String id, ChildDto childDto) {

		if (childDto == null)
			throw new NotNullException(ExceptionMessage.CHILD_MUST_NOT_NULL);

		if (childDto.getTaxNo() == null || childDto.getTaxNo().equals(""))
			throw new NotNullException(ExceptionMessage.TAXNO_MUST_NOT_NULL);

		ChildDto childTaxNo = findByTaxNo(childDto.getTaxNo());
		if (!(id.equals(childTaxNo.getPersonId())))
			throw new AlreadyExistException(ExceptionMessage.TAXNO_ALREADY_EXISTS);
	}

}
