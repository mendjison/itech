package kiwi.itech.repository;

import kiwi.itech.model.entities.Partner;
import kiwi.itech.repository.generic.JpaRepository;

import javax.ejb.Local;
import java.util.Optional;

@Local
public interface IPartnerDao  extends JpaRepository<Partner, String> {
    Optional<Partner> findPartnerByEmail(String email);
    Optional<Partner> findByTaxNo(String taxNo);
}
