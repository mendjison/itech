package kiwi.itech.model.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.JoinColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import kiwi.itech.model.entities.enums.GenderType;

@Entity

  @Table(uniqueConstraints={
  
  @UniqueConstraint(columnNames={"DTYPE","taxNo"})})
 
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Person implements Serializable {

	@Id
	@Column(length = 7)
	private String personId;
	@Column(length = 50)
	private String firstname;
	@Column(length = 50)
	private String lastname;
	@Column(length = 50)
	private String birthName;
	@Column(length = 50)
	private String email;
	@Enumerated(EnumType.STRING)
	@Column(length = 20)
	private GenderType gender;
	@JsonFormat(pattern = "yyyy-MM-dd")
	@JsonProperty("dateOfBirth")
	private LocalDate dateOfBirth;
	@Column(length = 50)
	private String taxNo;
	@Column(length = 50)
	private String nationality;
	@Column(length = 50)
	private String localOfBirth;
	@Column(length = 200)
	private String additionAddress;
	@ManyToOne (fetch = FetchType.LAZY)
	private Address address;
	@ElementCollection
	@CollectionTable(name = "Phone", joinColumns = @JoinColumn(name = "person"))
	private List<PhoneNumber> phones;

	public Person() {}


	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	public String getFirstname() {
		return firstname;
	}  

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public GenderType getGender() {
		return gender;
	}

	public void setGender(GenderType gender) {
		this.gender = gender;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getTaxNo() {
		return taxNo;
	}

	public void setTaxNo(String taxNo) {
		this.taxNo = taxNo;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getLocalOfBirth() {
		return localOfBirth;
	}

	public void setLocalOfBirth(String localOfBirth) {
		this.localOfBirth = localOfBirth;
	}

	public String getAdditionAddress() {
		return additionAddress;
	}

	public void setAdditionAddress(String additionAddress) {
		this.additionAddress = additionAddress;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public List<PhoneNumber> getPhones() {
		if (this.phones == null)
			this.phones = new ArrayList<>();
		return phones;
	}

	public void setPhones(List<PhoneNumber> phones) {
		this.phones = phones;
	}

	public void addPhone(PhoneNumber phone) {
		this.getPhones().add(phone);
	}

	public String getBirthName() {
		return birthName;
	}

	public void setBirthName(String birthName) {
		this.birthName = birthName;
	}

	@Override
	public String toString() {
		return "Person [personId=" + personId + ", firstname=" + firstname + ", lastname=" + lastname + ", birthName="
				+ birthName + ", email=" + email + ", gender=" + gender + ", dateOfBirth=" + dateOfBirth + ", taxNo="
				+ taxNo + ", nationality=" + nationality + ", localOfBirth=" + localOfBirth + ", additionAddress="
				+ additionAddress + ", address=" + address + ", phones=" + phones + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((additionAddress == null) ? 0 : additionAddress.hashCode());
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((birthName == null) ? 0 : birthName.hashCode());
		result = prime * result + ((dateOfBirth == null) ? 0 : dateOfBirth.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((localOfBirth == null) ? 0 : localOfBirth.hashCode());
		result = prime * result + ((nationality == null) ? 0 : nationality.hashCode());
		result = prime * result + ((personId == null) ? 0 : personId.hashCode());
		result = prime * result + ((phones == null) ? 0 : phones.hashCode());
		result = prime * result + ((taxNo == null) ? 0 : taxNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (additionAddress == null) {
			if (other.additionAddress != null)
				return false;
		} else if (!additionAddress.equals(other.additionAddress))
			return false;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (birthName == null) {
			if (other.birthName != null)
				return false;
		} else if (!birthName.equals(other.birthName))
			return false;
		if (dateOfBirth == null) {
			if (other.dateOfBirth != null)
				return false;
		} else if (!dateOfBirth.equals(other.dateOfBirth))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (gender != other.gender)
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (localOfBirth == null) {
			if (other.localOfBirth != null)
				return false;
		} else if (!localOfBirth.equals(other.localOfBirth))
			return false;
		if (nationality == null) {
			if (other.nationality != null)
				return false;
		} else if (!nationality.equals(other.nationality))
			return false;
		if (personId == null) {
			if (other.personId != null)
				return false;
		} else if (!personId.equals(other.personId))
			return false;
		if (phones == null) {
			if (other.phones != null)
				return false;
		} else if (!phones.equals(other.phones))
			return false;
		if (taxNo == null) {
			if (other.taxNo != null)
				return false;
		} else if (!taxNo.equals(other.taxNo))
			return false;
		return true;
	}
}
