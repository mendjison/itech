package unittest.repository.address;

import java.util.ArrayList;
import java.util.Arrays;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import kiwi.itech.repository.daoImpl.AddressDao;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import kiwi.itech.model.entities.Address;

@RunWith(MockitoJUnitRunner.class)
public class findAddressUnitTest {

	@Mock
	private EntityManager entityManager;
	@Mock
	private TypedQuery<Address> query;
	
	@InjectMocks
	private AddressDao addressRepository;
	
	@Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }
	
	@Test
	public void findAddressWhenNotExist() throws InstantiationException, IllegalAccessException {
		//given 
		Address address = new Address("Otto-Brenner-str.", "25", 57089, "Siegen");
		
		Mockito.when(entityManager.createNamedQuery("Address.findByStreetAndHouseNoAndByPostcodeAndCountry", Address.class))
		.thenReturn(query);
		
		Mockito.when(query.setParameter("street", address.getStreet())).thenReturn(query);
		Mockito.when(query.setParameter("houseNo", address.getHouseNo())).thenReturn(query);
		Mockito.when(query.setParameter("postcode", address.getPostcode())).thenReturn(query);
		Mockito.when(query.setParameter("country", address.getCountry())).thenReturn(query);
		
		Mockito.when(query.getResultList()).thenReturn(new ArrayList<Address>());
		//When
		Address result = addressRepository
				.findAddress(address.getStreet(), address.getHouseNo(),
						address.getPostcode(), address.getCountry());
		//then
		Assert.assertNull(result);
	}
	
	@Test
	public void findAddressWhenExist() throws InstantiationException, IllegalAccessException {
		//given 
		Address address = new Address("Otto-Brenner-str.", "25", 57089, "Siegen");
		
		Mockito.when(entityManager.createNamedQuery("Address.findByStreetAndHouseNoAndByPostcodeAndCountry", Address.class))
		.thenReturn(query);
		
		Mockito.when(query.setParameter("street", address.getStreet())).thenReturn(query);
		Mockito.when(query.setParameter("houseNo", address.getHouseNo())).thenReturn(query);
		Mockito.when(query.setParameter("postcode", address.getPostcode())).thenReturn(query);
		Mockito.when(query.setParameter("country", address.getCountry())).thenReturn(query);
		
		Mockito.when(query.getResultList()).thenReturn(new ArrayList<Address>(Arrays.asList(address)));
		//When
		Address result = addressRepository
				.findAddress(address.getStreet(), address.getHouseNo(),
						address.getPostcode(), address.getCountry());
		//then
		Assert.assertNotNull(result);
	}
}
