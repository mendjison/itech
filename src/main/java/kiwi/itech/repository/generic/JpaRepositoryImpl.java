package kiwi.itech.repository.generic;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

public class JpaRepositoryImpl<T extends Serializable, ID> implements JpaRepository<T, ID> {

    @PersistenceContext(name = "test")
    private EntityManager entityManager;

    public JpaRepositoryImpl() {
    }

    @Override
    public T save(T entity) {
         entityManager.persist(entity);
         entityManager.flush();
         return entity;
    }

    @Override
    public T update(T entity) {
        entityManager.merge(entity);
        entityManager.flush();
        return entity;
    }

    @Override
    public Optional<T> findById(ID id) {
        T entity = entityManager.find(getClassName(),id);
        return Optional.of(entity);
    }

    @Override
    public List<T> findAll() {
        return entityManager.createQuery("select c from " + getClassName().getSimpleName() + " c").getResultList();
    }

    @Override
    public boolean delete(T entity) {
         entityManager.remove(entity);
         return true;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    private Class<T> getClassName() {
        Type genericSuperClass = getClass().getGenericSuperclass();

        ParameterizedType parametrizedType = null;
        while (parametrizedType == null) {
            if ((genericSuperClass instanceof ParameterizedType)) {
                parametrizedType = (ParameterizedType) genericSuperClass;
            } else {
                genericSuperClass = ((Class<?>) genericSuperClass).getGenericSuperclass();
            }
        }

        return  (Class<T>) parametrizedType.getActualTypeArguments()[0];
    }
}
