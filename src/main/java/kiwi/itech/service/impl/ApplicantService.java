package kiwi.itech.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import kiwi.itech.kiwibackend.dtos.SearchAttribute;
import kiwi.itech.model.entities.Address;
import kiwi.itech.model.entities.Applicant;
import kiwi.itech.model.helper.ChildReferenceNoAndUsernameGenerator;
import kiwi.itech.model.helper.exception.AlreadyExistException;
import kiwi.itech.model.helper.exception.ExceptionMessage;
import kiwi.itech.model.helper.exception.NotNullException;
import kiwi.itech.repository.IAddressDao;
import kiwi.itech.repository.IApplicantDao;
import kiwi.itech.service.IAddressService;
import kiwi.itech.service.IApplicantService;
import kiwi.itech.kiwibackend.dtos.ApplicantDto;

@Stateless(name = "ApplicantService")
public class ApplicantService implements IApplicantService {

	@EJB(name = "ApplicantDao")
	private IApplicantDao applicantDao;
	@EJB(name = "ChildReferenceNoAndUsernameGenerator")
	private ChildReferenceNoAndUsernameGenerator childReferenceNoAndUsernameGenerator;
	@EJB(name = "AddressService")
	private IAddressService addressService;

	public ApplicantService() {}

	@Override
	public ApplicantDto saveApplicant(ApplicantDto applicantDto) {

		if (applicantDto == null)
			throw new NotNullException(ExceptionMessage.APPLICANT_MUST_NOT_NULL);

		if (applicantDto.getTaxNo() == null || applicantDto.getTaxNo().equals(""))
			throw new AlreadyExistException(ExceptionMessage.TAXNO_MUST_NOT_NULL);

		if (findByTaxNo(applicantDto.getTaxNo()) != null)
			throw new AlreadyExistException(ExceptionMessage.TAXNO_ALREADY_EXISTS);

		addressService.findOrSaveAddress(applicantDto);

		applicantDto.setPersonId(UUID.randomUUID().toString().substring(0,6));

		Applicant applicant = applicantDto.toPojo();

		applicant.setChildReferenceNo(childReferenceNoAndUsernameGenerator.childReferenceNoGenerator());

		childReferenceNoAndUsernameGenerator.generateUsernameAndPassword(applicant);

		return applicantDao.save(applicant).toDto();

	}

	@Override
	public ApplicantDto updateApplicant(String id, ApplicantDto applicantDto) {

		validationApplicantUpdate(id, applicantDto);
		Applicant applicant = applicantDao.findById(id).orElse(null);
		if(applicant == null)
			return null;

		return applicantDao.update(applicantDto.toPojo()).toDto();

	}

	@Override
	public List<ApplicantDto> findApplicants() {

		return applicantDao.findAll().stream()
				.map((Applicant applicant) -> applicant.toDto())
				.collect(Collectors.toList());
	}

	@Override
	public ApplicantDto findApplicant(String personId) {
		Applicant applicant = applicantDao.findById(personId).orElse(null);
		if(applicant == null)
			return null;

		return applicant.toDto();
	}

	@Override
	public boolean deleteApplicant(String personId) {
		Applicant applicant = applicantDao.findById(personId).orElse(null);
		if(applicant == null)
			return false;

		return applicantDao.delete(applicant);
	}

	@Override
	public ApplicantDto findByTaxNo(String taxNo) {
		Optional<Applicant> applicant = applicantDao.findByTaxNo(taxNo);
		return (applicant.isPresent())? applicant.get().toDto():null;
	}

	@Override
	public ApplicantDto findByEmail(String email) {
		Optional<Applicant> applicant = applicantDao.findByEmail(email);
		return (applicant.isPresent())? applicant.get().toDto():null;
	}
	
	@Override
	public ApplicantDto findByChildReferenceNo(String childReferenceNo) {
		Optional<Applicant> applicant = applicantDao.findByChildReferenceNo(childReferenceNo);
		return (applicant.isPresent())? applicant.get().toDto():null;
	}

	@Override
	public ApplicantDto saveApplicantfromApplicant(ApplicantDto applicantDto) {

		validationApplicant(applicantDto);

		Applicant applicant = applicantDao.save(applicantDto.toPojo());
		return applicant.toDto();
	}
	
	@Override
	public List<ApplicantDto> findByAddress(Address address) {
		return applicantDao.findByAddress(address)
				.stream()
				.map((Applicant applicant) -> applicant.toDto())
				.collect(Collectors.toList());
	}

	@Override
	public List<ApplicantDto> findByDateOfBirth(LocalDate dateOfBirth) {
		return applicantDao.findByDateOfBirth(dateOfBirth)
				.stream()
				.map((Applicant applicant) -> applicant.toDto())
				.collect(Collectors.toList());
	}

	@Override
	public List<ApplicantDto> findByFirtsnameAndLastname(String firstname, String lastname) {
		return applicantDao.findByFirtsnameAndLastname(firstname, lastname)
				.stream()
				.map((Applicant applicant) -> applicant.toDto())
				.collect(Collectors.toList());
	}

	@Override
	public List<ApplicantDto> searchApplicants(SearchAttribute searchAttribute) {
		
		return applicantDao.searchApplicants
				(searchAttribute)
				.stream()
				.map((Applicant applicant) -> applicant.toDto())
				.collect(Collectors.toList());
	}

	private void validationApplicant(ApplicantDto applicantDto) {

		if (applicantDto == null)
			throw new NotNullException(ExceptionMessage.APPLICANT_MUST_NOT_NULL);

		if (applicantDto.getTaxNo() == null || applicantDto.getTaxNo().equals(""))
			throw new AlreadyExistException(ExceptionMessage.TAXNO_MUST_NOT_NULL);

		if (findByTaxNo(applicantDto.getTaxNo()) != null)
			throw new AlreadyExistException(ExceptionMessage.TAXNO_ALREADY_EXISTS);

		if (applicantDto.getEmail() == null || applicantDto.getEmail().equals(""))
			throw new AlreadyExistException(ExceptionMessage.EMAIL_MUST_NOT_NULL);

		if (findByEmail(applicantDto.getEmail()) != null)
			throw new AlreadyExistException(ExceptionMessage.EMAIL_ALREADY_EXISTS);

		if (applicantDto.getPassword() == null || applicantDto.getPassword().equals(""))
			throw new AlreadyExistException(ExceptionMessage.PASSWORD_MUST_NOT_NULL);
	}

	private void validationApplicantUpdate(String applicantId, ApplicantDto applicantDto) {

		if (applicantDto == null)
			throw new NotNullException(ExceptionMessage.APPLICANT_MUST_NOT_NULL);

		if (applicantDto.getTaxNo() == null || applicantDto.getTaxNo().equals(""))
			throw new AlreadyExistException(ExceptionMessage.TAXNO_MUST_NOT_NULL);

		if (applicantDto.getEmail() == null || applicantDto.getEmail().equals(""))
			throw new AlreadyExistException(ExceptionMessage.EMAIL_MUST_NOT_NULL);

		ApplicantDto applicantTaxNo = findByTaxNo(applicantDto.getTaxNo());
		if ( !(applicantId.equals(applicantTaxNo.getPersonId())))
			throw new AlreadyExistException(ExceptionMessage.TAXNO_ALREADY_EXISTS);

		ApplicantDto applicantEmail = findByEmail(applicantDto.getTaxNo());
		if ( !(applicantId.equals(applicantEmail.getPersonId())))
			throw new AlreadyExistException(ExceptionMessage.EMAIL_ALREADY_EXISTS);
	}
}
