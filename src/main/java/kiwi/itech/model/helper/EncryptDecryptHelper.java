package kiwi.itech.model.helper;

public class EncryptDecryptHelper {

	private EncryptDecryptHelper() {
		super();
	}

	public static final String EncryptionKey           = "ABCDEFGHIJKLMNOP";
	public static final String CharacterEncoding       = "UTF-8";
	public static final String CipherTransformation    = "AES/CBC/PKCS5PADDING";
	public static final String AesEncryption = "AES";

}
