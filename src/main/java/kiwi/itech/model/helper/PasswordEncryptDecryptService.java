package kiwi.itech.model.helper;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.ejb.Stateless;

@Stateless(name = "PasswordEncryptDecryptService")
public class PasswordEncryptDecryptService {

	public PasswordEncryptDecryptService() {
		// TODO Auto-generated constructor stub
	}

	/**
     * Method for Encrypt Plain String Data
     * @param plainText
     * @return encryptedText
     */
    public static String encrypt(String plainText) {
        String encryptedText = "";
        try {
            Cipher cipher   = Cipher.getInstance(EncryptDecryptHelper.CipherTransformation);
            byte[] key      = EncryptDecryptHelper.EncryptionKey.getBytes(EncryptDecryptHelper.CharacterEncoding);
            SecretKeySpec secretKey = new SecretKeySpec(key, EncryptDecryptHelper.AesEncryption);
            IvParameterSpec ivparameterspec = new IvParameterSpec(key);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivparameterspec);
            byte[] cipherText = cipher.doFinal(plainText.getBytes("UTF8"));
            Base64.Encoder encoder = Base64.getEncoder();
            encryptedText = encoder.encodeToString(cipherText);

        } catch (Exception E) {
             System.err.println("Encrypt Exception : "+E.getMessage());
        }
        return encryptedText;
    }
    
    /**
     * Method For Get encryptedText and Decrypted provided String
     * @param encryptedText
     * @return decryptedText
     */
    public static String decrypt(String encryptedText) {
        String decryptedText = "";
        try {
            Cipher cipher = Cipher.getInstance(EncryptDecryptHelper.CipherTransformation);
            byte[] key = EncryptDecryptHelper.EncryptionKey.getBytes(EncryptDecryptHelper.CharacterEncoding);
            SecretKeySpec secretKey = new SecretKeySpec(key, EncryptDecryptHelper.AesEncryption);
            IvParameterSpec ivparameterspec = new IvParameterSpec(key);
            cipher.init(Cipher.DECRYPT_MODE, secretKey, ivparameterspec);
            Base64.Decoder decoder = Base64.getDecoder();
            byte[] cipherText = decoder.decode(encryptedText.getBytes("UTF8"));
            decryptedText = new String(cipher.doFinal(cipherText), "UTF-8");

        } catch (Exception E) {
            System.err.println("decrypt Exception : "+E.getMessage());
        }
        return decryptedText;
    }
}
