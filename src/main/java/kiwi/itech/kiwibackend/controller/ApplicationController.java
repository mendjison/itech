package kiwi.itech.kiwibackend.controller;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import io.swagger.annotations.Api;
import kiwi.itech.service.IApplicationService;
import kiwi.itech.kiwibackend.dtos.ApplicationDto;

@Path("applications")
@Consumes(MediaType.APPLICATION_JSON)
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
@Api("/kiwi applications")
//@SwaggerDefinition(tags = { @Tag(name = "kiwi application", description = "Controller for Application in Kiwi") })
public class ApplicationController {

	private Logger logger = Logger.getLogger(ApplicationController.class);
	@EJB(name = "ApplicationService")
	private IApplicationService applicationService;

	@POST
	public Response saveApplication(ApplicationDto applicationDto) {
		logger.info("ApplicationController.saveApplication is call");

		System.out.println(applicationDto.getApplicant().getDateOfBirth() + " Applicant +++++++++++++");


		return Response
				.status(Response.Status.OK)
				.entity(applicationService.createApplication(applicationDto))
				.build();
	}

	@PUT
	@Path(value = "/{applicationId}")
	public Response updateApplication(@PathParam("applicationId") String applicationId, ApplicationDto applicationDto) {
		return Response
				.status(Response.Status.OK)
				.entity(applicationService.updateApplication(applicationId, applicationDto))
				.build();
	}

	@DELETE
	@Path(value = "/{applicationId}")
	public Response deleteApplication(@PathParam("applicationId") String applicationId) {

		return Response.status(Response.Status.OK).entity(applicationService.deleteApplication(applicationId)).build();
	}

	@GET
	@Path(value = "/{applicationId}")
	public Response findApplication(@PathParam("applicationId") String applicationId) {

		return Response
				.status(Response.Status.OK)
				.entity(
						applicationService.findApplication(applicationId))
				.build();
	}

	@GET
	public List<ApplicationDto> findApplications() {
		logger.info("ApplicationController.findApplications is call");
		return applicationService.getApplications();
	}

	@GET
	@Path(value = "/applicant/{applicantId}")
	public List<ApplicationDto> findApplicationsByApplicantId(@PathParam("applicantId") String applicationId) {
		logger.info("ApplicationController.findApplicationsByApplicantId is call");
		return applicationService.getApplicationsByApplicant(applicationId);
	}

	@GET
	@Path(value = "/accept/{applicationId}")
	public Response accepteApplication(@PathParam("applicationId") String applicationId) {
		return Response
				.status(Response.Status.OK)
				.entity(applicationService.accepteApplication(applicationId))
				.build();
	}

	@GET 
	@Path(value = "/reject/{applicationId}")
	public Response rejectApplication(@PathParam("applicationId") String applicationId) {
		return Response
				.status(Response.Status.OK)
				.entity(applicationService.rejectApplication(applicationId))
				.build();
	}
}
