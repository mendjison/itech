package unittest.service.bankAccount;

import java.util.Optional;
import java.util.UUID;

import kiwi.itech.repository.IBankAccountDao;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import kiwi.itech.model.entities.BankAccount;
import kiwi.itech.model.entities.BankAccountOwner;
import kiwi.itech.kiwibackend.dtos.BankAccountDto;
import kiwi.itech.service.impl.BankAccountService;

@RunWith(MockitoJUnitRunner.class)
public class FindBankAccountByIdUnitTest {

	@Mock
    private IBankAccountDao bankAccountDao;
    @InjectMocks
    private BankAccountService bankAccountService;

    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }
    
    @Test
    public void whenNotFound() {
    	
        //given
        String id = UUID.randomUUID().toString();
        Mockito.when(bankAccountDao.findById(id))
                .thenReturn(Optional.empty());

        //when
        BankAccountDto bankAccountDtos = bankAccountService.findBankAccountById(id);
        //then
        Assert.assertNull(bankAccountDtos);
    }
    
    @Test
    public void whenOneFound() {
    	
        //given
        String id = UUID.randomUUID().toString();
        BankAccountDto bankAccountDto = new BankAccountDto().setBic("DEUTDEDBHAM")
                .setIban("DE682007002400428321000")
                .setAccountOwner(new BankAccountOwner())
                .setId(id);

        BankAccount bankAccount = new BankAccount();
        bankAccount.setAccountOwner(new BankAccountOwner());
        bankAccount.setBic("DEUTDEDBHAM");
        bankAccount.setIban("DE682007002400428321000");
        bankAccount.setId(id );
        
        Mockito.when(bankAccountDao.findById(id))
        .thenReturn(Optional.of(bankAccount));

        //when
        BankAccountDto resultat = bankAccountService.findBankAccountById(id);
        
        //then
        Assert.assertNotNull(resultat);
        //Assert.assertEquals("the id is 1", id, resultat.getId());
        //Assert.assertEquals("find object is bankAccountDto", bankAccountDto, resultat);;

    }
}
