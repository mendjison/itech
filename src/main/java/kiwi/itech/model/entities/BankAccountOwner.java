package kiwi.itech.model.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@SuppressWarnings("serial")
@Embeddable
public class BankAccountOwner implements Serializable{

	@Column(length = 50, nullable = true)
	private String ownerFirstname;
	@Column(length = 50, nullable = true)
	private String ownerLastname;
	public BankAccountOwner() {}
	BankAccountOwner(String ownerFirstname, String ownerLastname) {
		super();
		this.ownerFirstname = ownerFirstname;
		this.ownerLastname = ownerLastname;
	}
	public String getOwnerFirstname() {
		return ownerFirstname;
	}
	public void setOwnerFirstname(String ownerFirstname) {
		this.ownerFirstname = ownerFirstname;
	}
	public String getOwnerLastname() {
		return ownerLastname;
	}
	public void setOwnerLastname(String ownerLastname) {
		this.ownerLastname = ownerLastname;
	}
	@Override
	public String toString() {
		return "BankAccountOwner [ownerFirstname=" + ownerFirstname + ", ownerLastname=" + ownerLastname + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ownerFirstname == null) ? 0 : ownerFirstname.hashCode());
		result = prime * result + ((ownerLastname == null) ? 0 : ownerLastname.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankAccountOwner other = (BankAccountOwner) obj;
		if (ownerFirstname == null) {
			if (other.ownerFirstname != null)
				return false;
		} else if (!ownerFirstname.equals(other.ownerFirstname))
			return false;
		if (ownerLastname == null) {
			if (other.ownerLastname != null)
				return false;
		} else if (!ownerLastname.equals(other.ownerLastname))
			return false;
		return true;
	}
}
