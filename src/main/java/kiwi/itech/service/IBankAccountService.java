package kiwi.itech.service;

import java.util.List;

import javax.ejb.Local;

import kiwi.itech.kiwibackend.dtos.BankAccountDto;

@Local
public interface IBankAccountService {

	BankAccountDto findBankAccountByIban(String iban);
	BankAccountDto saveBankAccount(BankAccountDto account);
	BankAccountDto updateBankAccount(String id, BankAccountDto account);
	boolean deleteBankAccountById(String id);
	List<BankAccountDto> findBankAccounts();
	BankAccountDto findBankAccountById(String id);
}
