package kiwi.itech.model.entities;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import kiwi.itech.kiwibackend.dtos.AddressDto;
import kiwi.itech.kiwibackend.dtos.PartnerDto;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery(name = "Partner.findPartnerById", query = "select user from Partner as user where user.personId = :personId"),
	@NamedQuery(name = "Partner.findPartnerByEmail", query = "select user from Partner as user where user.email = :email"),
	@NamedQuery(name = "Partner.findPartners", query = "select user from Partner as user"),
	@NamedQuery(name = "Partner.findChildByTaxNo", query = "select user from Partner as user where user.taxNo = :taxNo"),
})
public class Partner extends Person {

	public Partner() {}


	public PartnerDto toDto() {
		return new PartnerDto()
				.setPersonId(getPersonId())
				.setFirstname(getFirstname())
				.setLastname(getLastname())
				.setEmail(getEmail())
				.setTaxNo(getTaxNo())
				.setBirthName(getBirthName())
				.setNationality(getNationality())
				.setLocalOfBirth(getLocalOfBirth())
				.setDateOfBirth(getDateOfBirth())
				.setAdditionAddress(getAdditionAddress())
				.setAddress((getAddress()==null|| getAddress().isNull()) ? new AddressDto() : getAddress().toDto())
				.setPhones(getPhones())
				.setGender(getGender());
	}

	public boolean isNull() {
		if(this.getEmail() == null
				&& this.getTaxNo() == null
				&& this.getFirstname() == null
				&& this.getLastname() == null)
			return true;
		return false;
	}

}
