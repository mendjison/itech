package kiwi.itech.service.impl;

import kiwi.itech.kiwibackend.dtos.PersonDto;
import kiwi.itech.model.entities.Address;
import kiwi.itech.repository.IAddressDao;
import kiwi.itech.service.IAddressService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.UUID;

@Stateless(name = "AddressService")
public class AddressService implements IAddressService {
    @EJB(name = "AddressDao")
    private IAddressDao addressDao;

    public AddressService() {
    }


    @Override
    public void findOrSaveAddress(PersonDto person) {

        if (person.getAddress() == null || person.getAddress().isNull())
            return;

        Address address = addressDao.findAddress(
                person.getAddress().getStreet(),
                person.getAddress().getHouseNo(),
                person.getAddress().getPostcode(),
                person.getAddress().getCountry());


        if (address == null) {
            person.getAddress().setAddressId(UUID.randomUUID().toString().substring(0,6));
            address = addressDao.save(person.getAddress().toPojo());
            person.setAddress(address.toDto());

        } else {
            person.getAddress().setAddressId(address.getAddressId());
        }
    }
}
