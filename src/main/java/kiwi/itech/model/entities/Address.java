package kiwi.itech.model.entities;

import kiwi.itech.kiwibackend.dtos.AddressDto;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints={
		@UniqueConstraint(columnNames={"houseNo","street","country", "postcode"})})
@NamedQueries({
	@NamedQuery(name = "Address.findByStreetAndHouseNoAndByPostcodeAndCountry", query = "select addres from Address as addres where addres.street = :street "
			+ "and addres.houseNo = :houseNo and addres.postcode = :postcode and addres.country = :country")
})
public class Address implements Serializable {

	@Id
	@Column(length = 7)
	private String addressId;
	@Column(length = 50)
	private String street;
	private String houseNo;
	private int postcode;
	@Column(length = 50)
	private String city;
	@Column(length = 50)
	private String country;
	public Address() {}
	public Address(String addressId, String street, String houseNo, int postcode, String city, String country) {
		super();
		this.addressId = addressId;
		this.street = street;
		this.houseNo = houseNo;
		this.postcode = postcode;
		this.city = city;
		this.country = country;
	}
	
	public Address(String street, String houseNo, int postcode, String city, String country) {
		this(UUID.randomUUID().toString(),street,  houseNo, postcode, city, country);
	}
	
	public Address(String street, String houseNo, int postcode, String city) {
		this(street,  houseNo, postcode, city, "Deutschland");
	}
	public String getAddressId() {
		return addressId;
	}
	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getHouseNo() {
		return houseNo;
	}
	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}
	public int getPostcode() {
		return postcode;
	}
	public void setPostcode(int postcode) {
		this.postcode = postcode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	public AddressDto toDto() {
		return new AddressDto()
				.setAddressId(getAddressId())
				.setStreet(getStreet())
				.setHouseNo(getHouseNo())
				.setPostcode(getPostcode())
				.setCity(getCity())
				.setCountry(getCountry());
	}
	@Override
	public String toString() {
		return "Address [addressId=" + addressId + ", street=" + street + ", houseNo=" + houseNo + ", postcode="
				+ postcode + ", city=" + city + ", country=" + country + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addressId == null) ? 0 : addressId.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((houseNo == null)? 0:houseNo.hashCode());
		result = prime * result + postcode;
		result = prime * result + ((street == null) ? 0 : street.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (addressId == null) {
			if (other.addressId != null)
				return false;
		} else if (!addressId.equals(other.addressId))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (houseNo != other.houseNo)
			return false;
		if (postcode != other.postcode)
			return false;
		if (street == null) {
			if (other.street != null)
				return false;
		} else if (!street.equals(other.street))
			return false;
		return true;
	}

	public boolean isNull() {
		if(this.getStreet() == null
				&& this.getHouseNo() == null
				&& this.getPostcode() == 0)
			return true;
		return false;
	}
}
