package kiwi.itech.model.entities;

import org.eclipse.persistence.annotations.UuidGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@MappedSuperclass
@UuidGenerator(name="EMP_ID_GEN")
public abstract  class BaseType implements Serializable {
    @Id
    @GeneratedValue
    @Type(type = "uuid-char")
    @Column(name = "id", updatable = false, nullable = false, length = 6)
    private UUID id;

    public UUID getId() {
        return id;
    }

    public void setId(UUID uuid) {
        this.id = uuid;
    }

    @Override
    public String toString() {
        return "\"id\":" + "\""+ getId().toString() + "\"";
    }
}
