package kiwi.itech.kiwibackend.dtos;

import java.time.LocalDate;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import kiwi.itech.model.entities.Address;

import javax.validation.constraints.Past;

public class SearchAttribute {

	private String username;
	private String childReferenceNo;
	private String taxNo;
	private String email;
	private Address address;
	@JsonDeserialize(as = LocalDate.class)
	@Past
	@JsonProperty("dateOfBirth")
	private LocalDate dateOfBirth;
	private String firstname;
	private String lastname;
	
	public SearchAttribute() {super();}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getChildReferenceNo() {
		return childReferenceNo;
	}
	public void setChildReferenceNo(String childReferenceNo) {
		this.childReferenceNo = childReferenceNo;
	}
	public String getTaxNo() {
		return taxNo;
	}
	public void setTaxNo(String taxNo) {
		this.taxNo = taxNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public LocalDate getDateOfBirth() {
		if(dateOfBirth == null)
			dateOfBirth = LocalDate.now();
		return dateOfBirth;
	}
	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@Override
	public String toString() {
		return "SearchAttribute [username=" + username + ", childReferenceNo=" + childReferenceNo + ", taxNo=" + taxNo
				+ ", email=" + email + ", address=" + address + ", dateOfBirth=" + dateOfBirth + ", firstname="
				+ firstname + ", lastname=" + lastname + "]";
	}
	
}
