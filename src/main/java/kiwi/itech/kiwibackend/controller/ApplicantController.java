package kiwi.itech.kiwibackend.controller;

import java.time.LocalDate;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.GenericEntity;


import org.apache.log4j.Logger;

import io.swagger.annotations.Api;
import kiwi.itech.kiwibackend.dtos.SearchAttribute;
import kiwi.itech.model.entities.Address;
import kiwi.itech.service.IApplicantService;
import kiwi.itech.kiwibackend.dtos.ApplicantDto;

@Path("applicants")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Api("/kiwi applicants")
//@SwaggerDefinition(tags = { @Tag(name = "kiwi applicants", description = "Controller for Applicant in kiwi") })
public class ApplicantController {

	@EJB(name = "ApplicantService")
	private IApplicantService applicantService;
	private Logger logger = Logger.getLogger(ApplicantController.class);

	@POST
	public ApplicantDto saveApplicant(ApplicantDto applicantDto) {
		return applicantService.saveApplicant(applicantDto);
	}

	@GET
	public List<ApplicantDto> findApplicants() {
		return applicantService.findApplicants();
	}

	@GET
	@Path("/{id}")
	public ApplicantDto findApplicant(@PathParam("id") String id) {
		logger.info("findApplicant is call");
		return applicantService.findApplicant(id);
	}

	@PUT
	@Path("/{id}")
	public ApplicantDto updateApplicant(@PathParam("id") String id, ApplicantDto applicantDto) {
		return applicantService.updateApplicant(id, applicantDto);
	}

	@DELETE
	@Path("/{applicantId}")
	public Response deleteApplicant(@PathParam("applicantId") String applicantId) {
		applicantService.deleteApplicant(applicantId);
		return Response.status(Response.Status.OK).build();
	}

	@GET
	@Path("/taxno/{taxNo}")
	public Response findByTaxNo(@PathParam("taxNo") String taxNo) {
		return Response
				.status(Response.Status.OK)
				.entity(applicantService.findByTaxNo(taxNo))
				.header("Access-Control-Allow-Origin", "*")
				.build();
	}

	@GET
	@Path("/email/{email}")
	public Response findByEmail(@PathParam("email") String email) {
		return Response.status(Response.Status.OK).entity(applicantService.findByEmail(email)).build();
	}

	@GET
	@Path("/childReferenceNo/{childReferenceNo}")
	public Response findByChildReferenceNo(@PathParam("childReferenceNo") String childReferenceNo) {
		return Response.status(Response.Status.OK).entity(applicantService.findByChildReferenceNo(childReferenceNo))
				.build();
	}

	// public Response saveApplicantfromApplicant(ApplicantDto applicantDto) {return null;}

	@GET
	@Path("/firstname")
	public List<ApplicantDto> findByFirtsnameAndLastname(@QueryParam("firstname") String firstname,
			@QueryParam("lastname") String lastname) {
		return applicantService.findByFirtsnameAndLastname(firstname, lastname);
	}

	@POST
	@Path("/dateOfBirth")
	public List<ApplicantDto> findByDateOfBirth(LocalDate dateOfBirth) {
		return applicantService.findByDateOfBirth(dateOfBirth);
	}

	@POST
	@Path("/address")
	public List<ApplicantDto> findByAddress(Address address) {
		return applicantService.findByAddress(address);
	}
	
	@POST
	@Path("/search")
	public /*List<ApplicantDto>*/ Response search(SearchAttribute searchAttribute) {
		List<ApplicantDto> res =  applicantService.searchApplicants(searchAttribute);

		 GenericEntity<List<ApplicantDto>> entity
				= new GenericEntity<List<ApplicantDto>>(res) {};

		return Response
		        .status(Response.Status.OK)
				.entity(entity)
				.header("Access-Control-Allow-Origin", "*")
				.build();
	}
}
