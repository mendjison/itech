package unittest.service.bankAccount;

import kiwi.itech.model.entities.BankAccount;
import kiwi.itech.model.entities.BankAccountOwner;
import kiwi.itech.repository.IBankAccountDao;
import kiwi.itech.kiwibackend.dtos.BankAccountDto;
import kiwi.itech.service.impl.BankAccountService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class FindBankAccountsUnitTest {

    @Mock
    private IBankAccountDao bankAccountDao;
    @InjectMocks
    private BankAccountService bankAccountService;

    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void whenNotFound() {
        //given
        Mockito.when(bankAccountDao.findAll())
                .thenReturn(new ArrayList<>());
        //when
        List<BankAccountDto> bankAccountDtos = bankAccountService.findBankAccounts();
        //then
        Assert.assertEquals(0, bankAccountDtos.size());

    }

    @Test
    public void whenOneFound() {
        //given
        BankAccountDto bankAccountDto = new BankAccountDto().setBic("DEUTDEDBHAM")
                .setIban("DE682007002400428321000")
                .setAccountOwner(new BankAccountOwner());

        BankAccount bankAccount = new BankAccount();
        bankAccount.setAccountOwner(new BankAccountOwner());
        bankAccount.setBic("DEUTDEDBHAM");
        bankAccount.setIban("DE682007002400428321000");
        
        Mockito.when(bankAccountDao.findAll())
                .thenReturn(new ArrayList<>(Arrays.asList(bankAccount)));
        //when
        List<BankAccountDto> bankAccountDtos = bankAccountService.findBankAccounts();
        //then
        Assert.assertEquals(1, bankAccountDtos.size());

    }
}
