package kiwi.itech.model.helper;

import java.util.Optional;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import kiwi.itech.model.entities.Applicant;
import kiwi.itech.repository.IApplicantDao;

@Stateless(name = "ChildReferenceNoAndUsernameGenerator")
public class ChildReferenceNoAndUsernameGenerator {

	@EJB(name = "ApplicantDao")
	private IApplicantDao applicantRepository;

	public ChildReferenceNoAndUsernameGenerator() {
		super();
	}

	public void generateUsernameAndPassword(Applicant applicant) {
		String[] firstname = applicant.getFirstname().split(" ");
		String[] lastname = applicant.getLastname().split(" ");
		char firstLetter = lastname[0].charAt(0);

		StringBuilder charIndexValue = new StringBuilder();
		charIndexValue.append(firstLetter);

		StringBuilder usernameBuilder = new StringBuilder();
		usernameBuilder.append(firstLetter).append(".").append(firstname[0]);
		String username = usernameBuilder.toString();

		int indexLastanme = 1;

		Optional<Applicant> applicantExist = applicantRepository.findByUsername(username.toString());

		while (applicantExist.isPresent() && indexLastanme < lastname[0].length()) {

			charIndexValue.append(lastname[0].charAt(indexLastanme));
			usernameBuilder = new StringBuilder();
			usernameBuilder
			.append(charIndexValue.toString())
			.append(".")
			.append(firstname[0]);
			username = usernameBuilder.toString();
			applicantExist = applicantRepository.findByUsername(username);
			indexLastanme++;
		}

		if(applicantExist.isPresent())
			username = username + Math.random()*10000;

		applicant.setPassword(generatePassword());
		applicant.setUsername(username);
		return ;
	}

	public String childReferenceNoGenerator() {
		String childReferenceNoGenerated = this.generator();
		Optional<Applicant> applicant = applicantRepository.findByChildReferenceNo(childReferenceNoGenerated);

		while (applicant.isPresent()) {
			childReferenceNoGenerated = this.generator();
			applicant = applicantRepository.findByChildReferenceNo(childReferenceNoGenerated);
		}

		return childReferenceNoGenerated;
	}

	private String generator() {
		StringBuilder childReferenceNoGenerator = new StringBuilder("");
		for (int i = 0; i < 9; i++) {

			if (i == 3)
				childReferenceNoGenerator.append("FK");

			childReferenceNoGenerator.append((int) (Math.random() * 9));
		}
		return childReferenceNoGenerator.toString();
	}

	private String generatePassword() {
		return UUID.randomUUID().toString().substring(0, 7);
	}
}
