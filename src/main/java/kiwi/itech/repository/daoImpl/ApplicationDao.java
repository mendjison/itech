package kiwi.itech.repository.daoImpl;

import kiwi.itech.model.entities.Applicant;
import kiwi.itech.model.entities.Application;
import kiwi.itech.repository.IApplicationDao;
import kiwi.itech.repository.generic.JpaRepositoryImpl;

import javax.ejb.Stateless;
import java.util.List;

@Stateless(name = "ApplicationDao")
public class ApplicationDao  extends JpaRepositoryImpl<Application, String> implements IApplicationDao {

    @Override
    public List<Application> findByApplicant(Applicant applicant) {
        return getEntityManager().createNamedQuery("Application.findApplicant", Application.class )
                .setParameter("personId", applicant.getPersonId())
                .getResultList();
    }
}
