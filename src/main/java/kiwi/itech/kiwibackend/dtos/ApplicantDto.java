package kiwi.itech.kiwibackend.dtos;

import java.time.LocalDate;
import java.util.List;

import kiwi.itech.model.entities.Applicant;
import kiwi.itech.model.entities.PhoneNumber;
import kiwi.itech.model.entities.enums.GenderType;

public class ApplicantDto extends PersonDto {

	private String childReferenceNo;
	private String username;
	private String password;

	public ApplicantDto() {
	}

	public String getChildReferenceNo() {
		return childReferenceNo;
	}

	public ApplicantDto setChildReferenceNo(String childReferenceNo) {
		this.childReferenceNo = childReferenceNo;
		return this;
	}

	public String getUsername() {
		return username;
	}

	public ApplicantDto setUsername(String username) {
		this.username = username;
		return this;
	}

	public String getPassword() {
		return password;
	}

	public ApplicantDto setPassword(String password) {
		this.password = password;
		return this;
	}

	@Override
	public String getFirstname() {
		return super.getFirstname();
	}

	@Override
	public ApplicantDto setFirstname(String firstname) {
		super.setFirstname(firstname);
		return this;
	}

	@Override
	public String getLastname() {
		return super.getLastname();
	}

	@Override
	public ApplicantDto setLastname(String lastname) {
		super.setLastname(lastname);
		return this;
	}

	@Override
	public String getBirthName() {
		return super.getBirthName();
	}

	@Override
	public ApplicantDto setBirthName(String birthName) {
		super.setBirthName(birthName);
		return this;
	}

	@Override
	public String getEmail() {
		return super.getEmail();
	}

	@Override
	public ApplicantDto setEmail(String email) {
		super.setEmail(email);
		return this;
	}

	@Override
	public GenderType getGender() {
		return super.getGender();
	}

	@Override
	public ApplicantDto setGender(GenderType gender) {
		super.setGender(gender);
		return this;
	}

	@Override
	public LocalDate getDateOfBirth() {
		return super.getDateOfBirth();
	}

	@Override
	public ApplicantDto setDateOfBirth(LocalDate dateOfBirth) {
		super.setDateOfBirth(dateOfBirth);
		return this;
	}

	@Override
	public String getTaxNo() {
		return super.getTaxNo();
	}

	@Override
	public ApplicantDto setTaxNo(String taxNo) {
		super.setTaxNo(taxNo);
		return this;
	}

	@Override
	public String getNationality() {
		return super.getNationality();
	}

	@Override
	public ApplicantDto setNationality(String nationality) {
		super.setNationality(nationality);
		return this;
	}

	@Override
	public String getLocalOfBirth() {
		return super.getLocalOfBirth();
	}

	@Override
	public ApplicantDto setLocalOfBirth(String localOfBirth) {
		super.setLocalOfBirth(localOfBirth);
		return this;
	}

	@Override
	public String getAdditionAddress() {
		return super.getAdditionAddress();
	}

	@Override
	public ApplicantDto setAdditionAddress(String additionAddress) {
		super.setAdditionAddress(additionAddress);
		return this;
	}

	@Override
	public AddressDto getAddress() {
		return super.getAddress();
	}

	@Override
	public ApplicantDto setAddress(AddressDto address) {
		super.setAddress(address);
		return this;
	}

	@Override
	public List<PhoneNumber> getPhones() {
		return super.getPhones();
	}

	@Override
	public ApplicantDto setPhones(List<PhoneNumber> phones) {
		super.setPhones(phones);
		return this;
	}

	@Override
	public String getPersonId() {
		return super.getPersonId();
	}

	@Override
	public ApplicantDto setPersonId(String personId) {
		super.setPersonId(personId);
		return this;
	}

	@Override
	public String toString() {
		return "ApplicantDto [childReferenceNo=" + childReferenceNo + ", username=" + username + ", password="
				+ password + ", getChildReferenceNo()=" + getChildReferenceNo() + ", getUsername()=" + getUsername()
				+ ", getPassword()=" + getPassword() + ", getFirstname()=" + getFirstname() + ", getLastname()="
				+ getLastname() + ", getBirthName()=" + getBirthName() + ", getEmail()=" + getEmail() + ", getGender()="
				+ getGender() + ", getDateOfBirth()=" + getDateOfBirth() + ", getTaxNo()=" + getTaxNo()
				+ ", getNationality()=" + getNationality() + ", getLocalOfBirth()=" + getLocalOfBirth()
				+ ", getAdditionAddress()=" + getAdditionAddress() + ", getAddress()=" + getAddress() + ", getPhones()="
				+ getPhones() + ", getPersonId()=" + getPersonId() + "]";
	}

	public Applicant toPojo() {
		Applicant applicant = new Applicant();
		applicant.setPersonId(getPersonId());
		applicant.setFirstname(getFirstname());
		applicant.setLastname(getLastname());
		applicant.setEmail(getEmail());
		applicant.setGender(getGender());
		applicant.setDateOfBirth(getDateOfBirth());
		applicant.setTaxNo(getTaxNo());
		applicant.setNationality(getNationality());
		applicant.setLocalOfBirth(getLocalOfBirth());
		applicant.setAdditionAddress(getAdditionAddress());
		applicant.setPhones(getPhones());
		applicant.setChildReferenceNo(getChildReferenceNo());
		applicant.setUsername(getUsername());
		applicant.setPassword(getPassword());

		if(getAddress() == null || getAddress().isNull()) {
			applicant.setAddress(null);
		}else {
			applicant.setAddress(getAddress().toPojo());
		}
		return applicant;
	}
	
}
