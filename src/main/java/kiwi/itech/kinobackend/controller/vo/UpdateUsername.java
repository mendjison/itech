package kiwi.itech.kinobackend.controller.vo;

public class UpdateUsername {

	private String username;
	private String newUsername;
	private String newUsernameConfirm;
	
	public UpdateUsername() {}
	
	
	
	public UpdateUsername(String username, String newUsername, String newUsernameConfirm) {
		super();
		this.username = username;
		this.newUsername = newUsername;
		this.newUsernameConfirm = newUsernameConfirm;
	}



	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getNewUsername() {
		return newUsername;
	}
	
	public void setNewUsername(String newUsername) {
		this.newUsername = newUsername;
	}
	public String getNewUsernameConfirm() {
		return newUsernameConfirm;
	}
	
	public void setNewUsernameConfirm(String newUsernameConfirm) {
		this.newUsernameConfirm = newUsernameConfirm;
	}
}
