package kiwi.itech.servletConfig;

import javax.ejb.Stateless;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import io.swagger.jaxrs.config.BeanConfig;

@Stateless
public class SwaggerServletConfig extends HttpServlet{

	
	  private static final long serialVersionUID = 1L; 
	  
	  public void init(ServletConfig config) throws ServletException {
	  super.init(config); BeanConfig beanConfig = new BeanConfig();
	  beanConfig.setBasePath("/backendKiwiKino/rest/api/");
	  beanConfig.setHost("localhost:7001");
	  beanConfig.setTitle("Kindergeld bei BA");
	  beanConfig.setResourcePackage("kiwi.itech");
	  beanConfig.setPrettyPrint(true);
	  beanConfig.setScan(true);
	  beanConfig.setSchemes(new String[]{"http"});
	  beanConfig.setVersion("1.0"); }

}
