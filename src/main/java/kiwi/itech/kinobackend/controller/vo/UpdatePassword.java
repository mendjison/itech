package kiwi.itech.kinobackend.controller.vo;

public class UpdatePassword {

	private String username;
	private String password;
	private String newPassword;
	private String newPasswordConfirm;
	
	public UpdatePassword() {}
	
	public UpdatePassword(String username, String password, String newPassword, String newPasswordConfirm) {
		this.username = username;
		this.password = password;
		this.newPassword = newPassword;
		this.newPasswordConfirm = newPasswordConfirm;
	}

	public String getUsername() {
		return username;
	}
	
	public UpdatePassword setUsername(String username) {
		this.username = username;
		return this;
	}
	
	public String getPassword() {
		return password;
	}
	
	public UpdatePassword setPassword(String password) {
		this.password = password;
		return this;
	}
	
	public String getNewPassword() {
		return newPassword;
	}
	
	public UpdatePassword setNewPassword(String newPassword) {
		this.newPassword = newPassword;
		return this;
	}

	public String getNewPasswordConfirm() {
		return newPasswordConfirm;
	}

	public void setNewPasswordConfirm(String newPasswordConfirm) {
		this.newPasswordConfirm = newPasswordConfirm;
	}
	
}
