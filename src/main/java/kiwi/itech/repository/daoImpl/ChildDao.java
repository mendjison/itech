package kiwi.itech.repository.daoImpl;

import kiwi.itech.model.entities.Child;
import kiwi.itech.repository.IChildDao;
import kiwi.itech.repository.generic.JpaRepositoryImpl;

import javax.ejb.Stateless;
import java.util.List;
import java.util.Optional;

@Stateless(name = "ChildDao")
public class ChildDao extends JpaRepositoryImpl<Child, String>  implements IChildDao {

    @Override
    public Optional<Child> findByTaxNo(String taxNo) {
        List<Child> childs = getEntityManager()
                .createNamedQuery("Child.findChildByTaxNo", Child.class)
                .setParameter("taxNo", taxNo).getResultList();

        return (childs == null || childs.isEmpty()) ? Optional.empty():Optional.of(childs.get(0));
    }
}
