package kiwi.itech.model.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.*;

import kiwi.itech.kiwibackend.dtos.PartnerDto;
import kiwi.itech.model.entities.enums.StatusType;
import kiwi.itech.kiwibackend.dtos.ApplicationDto;
import kiwi.itech.kiwibackend.dtos.ChildDto;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery(name = "Application.findAll", query = "select app from Application as app"),
	@NamedQuery(name = "Application.findApplicant", query = "select app from Application as app where app.applicant.personId = :personId")
})
public class Application implements Serializable{

	@Id
	@Column(length = 7)
	private String id;
	private LocalDate applicationDate;
	private StatusType status;
	@ManyToOne(fetch = FetchType.LAZY)
	private Applicant applicant;
	@ManyToOne(fetch = FetchType.LAZY)
	private Partner partner;
	@OneToMany(fetch = FetchType.LAZY)
	private List<Child> childs;
	@ManyToOne(fetch = FetchType.LAZY)
	private BankAccount account;
	public Application() {}
	public Application(LocalDate applicationDate, StatusType status, Applicant applicant, Partner partner, List<Child> childs,
			BankAccount account) {
		super();
		this.applicationDate = applicationDate;
		this.status = status;
		this.applicant = applicant;
		this.childs = childs;
		this.account = account;
		this.partner = partner;
	}
	
	public Application(LocalDate applicationDate, StatusType status, Applicant applicant, Child child,
			BankAccount account) {
		this.applicationDate = applicationDate;
		this.status = status;
		this.applicant = applicant;
		this.addChild(child);
		this.account = account;
	}
	
	
	public Application(Applicant applicant, List<Child> childs, BankAccount account) {
		this(LocalDate.now(), StatusType.CREATE, applicant, null,childs, account);
	}
	
	public Application(Applicant applicant, Child child, BankAccount account) {
		this(LocalDate.now(), StatusType.CREATE, applicant, child, account);
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public LocalDate getApplicationDate() {
		return applicationDate;
	}
	public void setApplicationDate(LocalDate applicationDate) {
		this.applicationDate = applicationDate;
	}
	public StatusType getStatus() {
		return status;
	}
	public void setStatus(StatusType status) {
		this.status = status;
	}
	
	public Applicant getApplicant() {
		return applicant;
	}
	public void setApplicant(Applicant applicant) {
		this.applicant = applicant;
	}
	public List<Child> getChilds() {
		if(this.childs == null)
			this.childs = new ArrayList<>();
		return childs;
	}
	public void setChilds(List<Child> childs) {
		this.childs = childs;
	}
	public BankAccount getAccount() {
		return account;
	}
	public void setAccount(BankAccount account) {
		this.account = account;
	}
	
	public void addChild(Child child) {
		this.getChilds().add(child);
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	@Override
	public String toString() {
		return "Application [id=" + id
				+ ", applicationDate=" + applicationDate
				+ ", status=" + status
				+ ", applicant=" + applicant
				+ ", partner=" + partner
				+ ", childs=" + childs
				+ ", account="
				+ account + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Application)) return false;
		Application that = (Application) o;
		return getId().equals(that.getId()) &&
				getApplicationDate().equals(that.getApplicationDate()) &&
				getStatus() == that.getStatus() &&
				getApplicant().equals(that.getApplicant()) &&
				partner.equals(that.partner) &&
				getChilds().equals(that.getChilds()) &&
				getAccount().equals(that.getAccount());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId(), getApplicationDate(), getStatus(), getApplicant(), partner, getChilds(), getAccount());
	}

	public ApplicationDto toDto() {

		List<ChildDto> childDtos = new ArrayList<>();

		getChilds().stream()
				.forEach((Child child) -> childDtos.add(child.toDto()));

		return  new ApplicationDto()
				.setAccount(getAccount().toDto())
				.setApplicant(getApplicant().toDto())
				.setApplicationDate(getApplicationDate())
				.setStatus(getStatus())
				.setPartner((getPartner()==null|| getPartner().isNull())? new PartnerDto() :getPartner().toDto())
				.setId(getId())
				.setChilds(childDtos);
	}
}
