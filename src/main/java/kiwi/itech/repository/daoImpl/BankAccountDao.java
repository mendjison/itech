package kiwi.itech.repository.daoImpl;

import kiwi.itech.model.entities.BankAccount;
import kiwi.itech.repository.IBankAccountDao;
import kiwi.itech.repository.generic.JpaRepositoryImpl;

import javax.ejb.Stateless;
import java.util.List;
import java.util.Optional;
@Stateless(name = "BankAccountDao")
public class BankAccountDao extends JpaRepositoryImpl<BankAccount, String> implements IBankAccountDao {

    @Override
    public Optional<BankAccount> findByIban(String iban) {
        List<BankAccount> accounts = getEntityManager()
                .createNamedQuery("BankAccount.findByIban", BankAccount.class)
                .setParameter("iban", iban)
                .getResultList();
        return (accounts == null || accounts.isEmpty()) ? Optional.empty(): Optional.of(accounts.get(0));
    }
}
