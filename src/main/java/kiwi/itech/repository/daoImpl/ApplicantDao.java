package kiwi.itech.repository.daoImpl;

import kiwi.itech.kiwibackend.dtos.SearchAttribute;
import kiwi.itech.model.entities.Address;
import kiwi.itech.model.entities.Applicant;
import kiwi.itech.repository.IApplicantDao;
import kiwi.itech.repository.generic.JpaRepositoryImpl;

import javax.ejb.Stateless;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Stateless(name = "ApplicantDao")
public class ApplicantDao extends JpaRepositoryImpl<Applicant, String> implements IApplicantDao {

    @Override
    public Optional<Applicant> findByUsername(String username) {
        List<Applicant> applicants = getEntityManager()
                .createNamedQuery("Applicant.findByUsername", Applicant.class)
                .setParameter("username", username).getResultList();
        return (applicants == null || applicants.isEmpty()) ? Optional.empty() : Optional.of(applicants.get(0));
    }

    @Override
    public Optional<Applicant> findByChildReferenceNo(String childReferenceNo) {
        List<Applicant> applicants = getEntityManager()
                .createNamedQuery("Applicant.findByChildReferenceNo", Applicant.class)
                .setParameter("childReferenceNo", childReferenceNo).getResultList();

        return (applicants == null || applicants.isEmpty()) ? Optional.empty() : Optional.of(applicants.get(0));
    }

    @Override
    public Optional<Applicant> findByTaxNo(String taxNo) {
        List<Applicant> applicants = getEntityManager()
                .createNamedQuery("Applicant.findByTaxNo", Applicant.class)
                .setParameter("taxNo", taxNo).getResultList();

        return (applicants == null || applicants.isEmpty()) ? Optional.empty() : Optional.of(applicants.get(0));
    }

    @Override
    public Optional<Applicant> findByEmail(String email) {
        List<Applicant> applicants = getEntityManager()
                .createNamedQuery("Applicant.findByEmail", Applicant.class)
                .setParameter("email", email).getResultList();

        return (applicants == null || applicants.isEmpty()) ? Optional.empty() : Optional.of(applicants.get(0));
    }

    @Override
    public List<Applicant> findByAddress(Address address) {
        return getEntityManager()
                .createNamedQuery("Applicant.findAddress", Applicant.class)
                .setParameter("address", address)
                .getResultList();
    }

    @Override
    public List<Applicant> findByDateOfBirth(LocalDate dateOfBirth) {
        return getEntityManager()
                .createNamedQuery("Applicant.findByDateOfBirth", Applicant.class)
                .setParameter("dateOfBirth", dateOfBirth)
                .getResultList();
    }

    @Override
    public List<Applicant> findByFirtsnameAndLastname(String firstname, String lastname) {
        return getEntityManager()
                .createNamedQuery("Applicant.findByFirtsnameAndLastname", Applicant.class)
                .setParameter("firstname", firstname)
                .setParameter("lastname", lastname)
                .getResultList();
    }

    @Override
    public List<Applicant> searchApplicants(SearchAttribute searchAttribute) {

        return getEntityManager().createNamedQuery("Applicant.search", Applicant.class)
                .setParameter("firstname", searchAttribute.getFirstname())
                .setParameter("lastname", searchAttribute.getLastname())
                .setParameter("username", searchAttribute.getUsername())
                .setParameter("childReferenceNo", searchAttribute.getChildReferenceNo())
                .setParameter("taxNo", searchAttribute.getTaxNo())
                .setParameter("email", searchAttribute.getEmail())
                .setParameter("address", searchAttribute.getAddress())
                //.setParameter("dateOfBirth", searchAttribute.DateOfBirth())
                .getResultList();
    }
}
