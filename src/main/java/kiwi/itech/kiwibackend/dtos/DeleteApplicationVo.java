package kiwi.itech.kiwibackend.dtos;

public class DeleteApplicationVo {

	private String applicationId;
	private boolean isDeleted;

	public DeleteApplicationVo(String applicationId, boolean isDeleted) {
		super();
		this.applicationId = applicationId;
		this.isDeleted = isDeleted;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Override
	public String toString() {
		return "DeleteApplicationVo [applicationId=" + applicationId + ", isDeleted=" + isDeleted + "]";
	}
	
	
}
