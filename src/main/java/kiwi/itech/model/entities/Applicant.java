package kiwi.itech.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import kiwi.itech.kiwibackend.dtos.AddressDto;
import kiwi.itech.kiwibackend.dtos.ApplicantDto;

@Entity
@NamedQueries({
	@NamedQuery(name = "Applicant.findById", query = "select user from Applicant as user where user.personId = :personId"),
	@NamedQuery(name = "Applicant.findAll", query = "select user from Applicant as user"),
	@NamedQuery(name = "Applicant.findByUsername", query = "select user from Applicant as user where user.username = :username"),
	@NamedQuery(name = "Applicant.findByChildReferenceNo", query = "select user from Applicant  as user where user.childReferenceNo = :childReferenceNo"),
	@NamedQuery(name = "Applicant.findByTaxNo", query = "select user from Applicant as user where user.taxNo = :taxNo"),
	@NamedQuery(name = "Applicant.findByEmail", query = "select user from Applicant as user where user.email = :email"),
	@NamedQuery(name = "Applicant.findAddress", query = "select user from Applicant as user where user.address = :address"),
	@NamedQuery(name = "Applicant.findByDateOfBirth", query = "select user from Applicant as user where user.dateOfBirth = :dateOfBirth"),
	@NamedQuery(name = "Applicant.search",
	query = "select user from Applicant as user where user.username = :username or user.childReferenceNo = :childReferenceNo"
			+ " or user.taxNo = :taxNo or user.email = :email or user.address = :address "
			+ " or user.firstname = :firstname or user.lastname = :lastname"),
	@NamedQuery(name = "Applicant.findByFirtsnameAndLastname",
	query = "select user from Applicant as user where user.firstname = :firstname or user.lastname = :lastname"),
})
//or user.dateOfBirth = :dateOfBirth
public class Applicant extends Person{

	@Column(length = 50, nullable = false, unique = true)
	private String childReferenceNo;
	@Column(length = 50, nullable = false, unique = true)
	private String username;
	@Column(length = 150, nullable = false)
	private String password;
	public Applicant() {}
	public Applicant(String childReferenceNo, String username, String password) {
		super();
		this.childReferenceNo = childReferenceNo;
		this.username = username;
		this.password = password;
	}


	public ApplicantDto toDto() {
		return new ApplicantDto()
				.setPersonId(getPersonId())
				.setFirstname(getFirstname())
				.setLastname(getLastname())
				.setEmail(getEmail())
				.setTaxNo(getTaxNo())
				.setBirthName(getBirthName())
				.setNationality(getNationality())
				.setLocalOfBirth(getLocalOfBirth())
				.setDateOfBirth(getDateOfBirth())
				.setAdditionAddress(getAdditionAddress())
				.setAddress((getAddress()==null || getAddress().isNull())? new AddressDto() :getAddress().toDto())
				.setPhones(getPhones())
				.setPassword(getPassword())
				.setUsername(getUsername())
				.setChildReferenceNo(getChildReferenceNo())
				.setGender(getGender());
	}

	
	public String getChildReferenceNo() {
		return childReferenceNo;
	}
	public void setChildReferenceNo(String childReferenceNo) {
		this.childReferenceNo = childReferenceNo;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "Applicant [childReferenceNo=" + childReferenceNo + ", username=" + username + ", password=" + password
				+ "]"
				+ super.toString();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((childReferenceNo == null) ? 0 : childReferenceNo.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Applicant other = (Applicant) obj;
		if (childReferenceNo == null) {
			if (other.childReferenceNo != null)
				return false;
		} else if (!childReferenceNo.equals(other.childReferenceNo))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
}
