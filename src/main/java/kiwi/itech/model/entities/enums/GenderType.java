package kiwi.itech.model.entities.enums;

public enum GenderType {
	MAN,
	WOMAN
}
