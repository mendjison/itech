package kiwi.itech.service.impl;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import kiwi.itech.model.entities.Partner;
import kiwi.itech.model.helper.exception.AlreadyExistException;
import kiwi.itech.model.helper.exception.ExceptionMessage;
import kiwi.itech.model.helper.exception.NotNullException;
import kiwi.itech.repository.IPartnerDao;
import kiwi.itech.service.IAddressService;
import kiwi.itech.service.IPartnerService;
import kiwi.itech.kiwibackend.dtos.PartnerDto;

@Stateless(name = "PartnerService")
public class PartnerService implements IPartnerService{

	@EJB(name = "PartnerDao")
	private IPartnerDao partnerDao;
	@EJB(name = "AddressService")
	private IAddressService addressService;
	
	public PartnerService() {}

	@Override
	public PartnerDto findPartnerById(String partnerId) {
		Partner partner =  partnerDao.findById(partnerId).orElse(null);
		if(partner == null)
			return null;
		return partner.toDto();
	}

	@Override
	public PartnerDto findPartnerByEmail(String email) {
		Partner partner =  partnerDao.findPartnerByEmail(email).orElse(null);
		if(partner == null)
			return null;
		return partner.toDto();
	}

	@Override
	public PartnerDto savePartner(PartnerDto partnerDto) {
		
		validationApplicant(partnerDto);
		addressService.findOrSaveAddress(partnerDto);

		if(partnerDto.isNull()) {
			return null;
		}

		partnerDto.setPersonId(UUID.randomUUID().toString().substring(0,6));
		return partnerDao.save(partnerDto.toPojo()).toDto();
	}

	@Override
	public PartnerDto updatePartner(String partnerId, PartnerDto partnerDto) {
		
		validationApplicantUpdate(partnerId, partnerDto);
		Partner partner = partnerDao.findById(partnerId).orElse(null);
		if(partner == null)
			return savePartner(partnerDto);
		addressService.findOrSaveAddress(partnerDto);
		return partnerDao
				.update(partnerDto
								.setPersonId(partnerId)
								.toPojo())
				.toDto();
	}

	@Override
	public boolean deletePartner(String partnerId) {
		Partner partner = partnerDao.findById(partnerId).orElse(null);
		if(partner == null)
			return false;
		return partnerDao.delete(partner);
	}

	@Override
	public List<PartnerDto> findPartnes() {
		return partnerDao.findAll()
				.stream()
				.map((Partner partner) -> partner.toDto())
				.collect(Collectors.toList());
	}
	
	@Override
	public PartnerDto findByTaxNo(String taxNo) {
		Partner partner = partnerDao.findByTaxNo(taxNo).orElse(null);
		if(partner == null)
			return null;
		return partner.toDto();
	}
	
	private void validationApplicant(PartnerDto partnerDto) {

		if (partnerDto == null || partnerDto.isNull())
			return;

		if (partnerDto.getTaxNo() == null || partnerDto.getTaxNo().equals(""))
			throw new AlreadyExistException(ExceptionMessage.TAXNO_MUST_NOT_NULL);

		if (findByTaxNo(partnerDto.getTaxNo()) != null)
			throw new AlreadyExistException(ExceptionMessage.TAXNO_ALREADY_EXISTS);

		if (partnerDto.getEmail() == null || partnerDto.getEmail().equals(""))
			throw new AlreadyExistException(ExceptionMessage.EMAIL_MUST_NOT_NULL);

		if (findPartnerByEmail(partnerDto.getEmail()) != null)
			throw new AlreadyExistException(ExceptionMessage.EMAIL_ALREADY_EXISTS);
	}

	private void validationApplicantUpdate(String partnerId, PartnerDto partnerDto) {

		if (partnerDto == null || partnerDto.isNull())
			return;

		if (partnerDto.getTaxNo() == null || partnerDto.getTaxNo().equals(""))
			throw new AlreadyExistException(ExceptionMessage.EMAIL_MUST_NOT_NULL);

		if (partnerDto.getEmail() == null || partnerDto.getEmail().equals(""))
			throw new AlreadyExistException(ExceptionMessage.TAXNO_MUST_NOT_NULL);

		PartnerDto partnerTaxNo = findByTaxNo(partnerDto.getTaxNo());
		if ( !(partnerId.equals(partnerTaxNo.getPersonId())))
			throw new AlreadyExistException(ExceptionMessage.TAXNO_ALREADY_EXISTS);

		PartnerDto partnerEmail = findPartnerByEmail(partnerDto.getTaxNo());
		if ( !(partnerId.equals(partnerEmail.getPersonId())))
			throw new AlreadyExistException(ExceptionMessage.EMAIL_ALREADY_EXISTS);
	}
}
