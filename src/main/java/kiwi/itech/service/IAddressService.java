package kiwi.itech.service;

import kiwi.itech.kiwibackend.dtos.PersonDto;

import javax.ejb.Local;

@Local
public interface IAddressService {
    void findOrSaveAddress(PersonDto person);
}
