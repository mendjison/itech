package kiwi.itech.model.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import kiwi.itech.model.entities.enums.PhoneType;

@SuppressWarnings("serial")
@Embeddable
public class PhoneNumber implements Serializable{
	@Column(length = 5, nullable = false)
	private String countryCode;
	@Column(length = 5, nullable = false)
	private String prefix;
	@Column(length = 10, nullable = false)
	private String sufix;
	@Enumerated(EnumType.STRING)
	private PhoneType phoneType;
	public PhoneNumber() {}
	public PhoneNumber(String countryCode, String prefix, String sufix, PhoneType phoneType ) {
		super();
		this.countryCode = countryCode;
		this.prefix = prefix;
		this.sufix = sufix;
		this.phoneType = phoneType;
	}
	
	public PhoneNumber( String prefix, String sufix, PhoneType phoneType) {
		this("0049", prefix, sufix, phoneType);
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getSufix() {
		return sufix;
	}
	public void setSufix(String sufix) {
		this.sufix = sufix;
	}
	public PhoneType getPhoneType() {
		return phoneType;
	}
	public void setPhoneType(PhoneType phoneType) {
		this.phoneType = phoneType;
	}
	@Override
	public String toString() {
		return "PhoneNumber [countryCode=" + countryCode + ", prefix=" + prefix + ", sufix=" + sufix + ", phoneType="
				+ phoneType + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((countryCode == null) ? 0 : countryCode.hashCode());
		result = prime * result + ((phoneType == null) ? 0 : phoneType.hashCode());
		result = prime * result + ((prefix == null) ? 0 : prefix.hashCode());
		result = prime * result + ((sufix == null) ? 0 : sufix.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PhoneNumber other = (PhoneNumber) obj;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (phoneType != other.phoneType)
			return false;
		if (prefix == null) {
			if (other.prefix != null)
				return false;
		} else if (!prefix.equals(other.prefix))
			return false;
		if (sufix == null) {
			if (other.sufix != null)
				return false;
		} else if (!sufix.equals(other.sufix))
			return false;
		return true;
	}
	
}
