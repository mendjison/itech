package kiwi.itech.service;

import java.util.List;

import javax.ejb.Local;

import kiwi.itech.kiwibackend.dtos.ApplicationDto;

@Local
public interface IApplicationService {

	ApplicationDto createApplication(ApplicationDto applicationDto);
	ApplicationDto createApplicationFromApplicant(String applicantId, ApplicationDto applicationDto);
	ApplicationDto updateApplication(String applicationId, ApplicationDto applicationDto);
	ApplicationDto updateApplicationFromApplicant(String applicationId, ApplicationDto applicationDto);
	List<ApplicationDto> getApplications();
	boolean deleteApplication(String applicationId);
	ApplicationDto findApplication(String applicationId);
	
	List<ApplicationDto> getApplicationsByApplicant(String applicationId);
	
	ApplicationDto accepteApplication(String applicationId);
	ApplicationDto rejectApplication(String applicationId);
}
