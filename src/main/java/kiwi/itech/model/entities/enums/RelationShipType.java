package kiwi.itech.model.entities.enums;

public enum RelationShipType {
	BIOLOGICALCHILD, ADOPTEDCHILD, FOSTERCHILD, STEPCHILD, GRANDCHILD
}
