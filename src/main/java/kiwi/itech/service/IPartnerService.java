package kiwi.itech.service;

import java.util.List;

import javax.ejb.Local;

import kiwi.itech.kiwibackend.dtos.PartnerDto;

@Local
public interface IPartnerService {

	PartnerDto findPartnerById(String partnerId);
	PartnerDto findPartnerByEmail(String email);
	PartnerDto savePartner(PartnerDto partnerDto);
	PartnerDto updatePartner(String partnerId, PartnerDto partnerDto);
	boolean deletePartner(String partnerId);
	List<PartnerDto> findPartnes();
	PartnerDto findByTaxNo(String taxNo);
}
