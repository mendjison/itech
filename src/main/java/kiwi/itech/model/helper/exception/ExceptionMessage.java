package kiwi.itech.model.helper.exception;

public class ExceptionMessage {

	public static final String APPLICANT_NOT_FOUND = "Applicant not found";
	public static final String APPLICANT_WITH_USERNAME_NOT_FOUND = "Applicant with username not found";
	public static final String APPLICANT_CHILDREFERENCENO_NOT_FOUND= "Applicant with this childReferenceNo not found";
	public static final String APPLICANT_ALREADY_EXISTS = "Applicant already exists";
	public static final String USERNAME_ALREADY_EXISTS = "Username already exists";
	public static final String TAXNO_ALREADY_EXISTS = "TaxNo already exists";
	public static final String TAXNO_MUST_NOT_NULL = "TaxNo must not null";
	public static final String EMAIL_ALREADY_EXISTS = "Email already exists";
	public static final String EMAIL_MUST_NOT_NULL = "Email must not null";
	public static final String PASSWORD_MUST_NOT_NULL = "Password must not null";
	public static final String IBAN_MUST_NOT_NULL = "Iban must not null";
	public static final String IBAN_ALREADY_EXISTS = "Iban already exists";
	public static final String BIC_MUST_NOT_NULL = "bic must not null";
	
	public static final String APPLICANT_MUST_NOT_NULL = "Applicant must not null";
	public static final String CHILD_MUST_NOT_NULL = "Child must not null";
	public static final String PARTNER_MUST_NOT_NULL = "Partner must not null";
	public static final String APPLICATION_MUST_NOT_NULL = "Application must not null";
	public static final String BANKACCOUNT_MUST_NOT_NULL = "Bank´s account must not null";
}
