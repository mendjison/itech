package kiwi.itech.model.entities.enums;

public enum PhoneType {

	PRIVATE, MOBIL, TELEPHON, WORK
}
