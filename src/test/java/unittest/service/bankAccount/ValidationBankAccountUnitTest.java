package unittest.service.bankAccount;

import kiwi.itech.repository.IBankAccountDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import kiwi.itech.model.entities.BankAccountOwner;
import kiwi.itech.model.helper.exception.NotNullException;
import kiwi.itech.kiwibackend.dtos.BankAccountDto;
import kiwi.itech.service.impl.BankAccountService;

@RunWith(MockitoJUnitRunner.class)
public class ValidationBankAccountUnitTest {

	@Mock
    private IBankAccountDao bankAccountDao;
    
    @InjectMocks
    @Spy
    private BankAccountService bankAccountService;

    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }
    
    @Test(expected = NotNullException.class)
    public void whenBankAccountIsNull() {
        //given
    	BankAccountDto accountDto = null;
        //when
    	
        bankAccountService.validationBankAccount(accountDto);
        //then
        //Assert.assertEquals(0, bankAccountDtos.size());
    }
    
    @Test(expected = NotNullException.class)
    public void whenIbanIsNull() {
        //given
    	BankAccountDto accountDto = new  BankAccountDto();
        //when
    	
        bankAccountService.validationBankAccount(accountDto);
    }
    
    @Test(expected = NotNullException.class)
    public void whenIbanAlreadyExist() {
        //given
    	BankAccountDto accountDto = new BankAccountDto().setBic("DEUTDEDBHAM")
                .setIban("DE682007002400428321000")
                .setAccountOwner(new BankAccountOwner());
    	
    	Mockito.doReturn(accountDto).when(bankAccountService).findBankAccountByIban(accountDto.getIban());
        //when
    	
        bankAccountService.validationBankAccount(accountDto);
    }
}
