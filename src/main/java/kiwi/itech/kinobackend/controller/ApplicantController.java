package kiwi.itech.kinobackend.controller;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import io.swagger.annotations.Api;
import kiwi.itech.kinobackend.controller.vo.UpdatePassword;
import kiwi.itech.kinobackend.controller.vo.UpdateUsername;
import kiwi.itech.service.IApplicantService;
import kiwi.itech.service.IApplicationService;
import kiwi.itech.kiwibackend.dtos.ApplicantDto;
import kiwi.itech.kiwibackend.dtos.ApplicationDto;

@Path("/kino/applicants")
@Consumes(MediaType.APPLICATION_JSON)
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
@Api("/kino applicants")
//@SwaggerDefinition(tags = { @Tag(name = "Kino applicants", description = "Controller for Applicant in Kino") })
public class ApplicantController {

	@EJB(name = "ApplicantService")
	private IApplicantService applicantService;

	@EJB(name = "ApplicationService")
	private IApplicationService applicationService;

	@POST
	public ApplicantDto saveApplicant(ApplicantDto applicantDto) {
		return applicantService.saveApplicantfromApplicant(applicantDto);
	}

	@GET
	@Path("/{applicantId}")
	public ApplicantDto findApplicant(@PathParam("applicantId") String applicantId) {
		return applicantService.findApplicant(applicantId);
	}

	@PUT
	@Path("/{applicantId}")
	public ApplicantDto updateApplicant(@PathParam("applicantId") String applicantId, ApplicantDto applicantDto) {
		return applicantService.updateApplicant(applicantId, applicantDto);
	}

	@PUT
	@Path("/{applicantId}/changePassword")
	public ApplicantDto updatePassword(@PathParam("applicantId") String applicantId, UpdatePassword updatePassword) {
		return null;
	}

	@PUT
	@Path("/{applicantId}/changeUsername")
	public ApplicantDto updateUsername(@PathParam("applicantId") String applicantId, UpdateUsername updateUsername) {
		return null;
	}

	@PUT
	@Path("/{applicantId}/applications")
	public Response updateApplications(@PathParam("applicantId") String applicantId, ApplicationDto applicationDto) {

		return Response.status(Status.OK)
				.entity(applicationService.createApplicationFromApplicant(applicantId, applicationDto)).build();
	}

	@POST
	@Path("/{applicantId}/applications")
	public Response saveApplications(@PathParam("applicantId") String applicantId, ApplicationDto applicationDto) {

		return Response.status(Status.OK)
				.entity(applicationService.createApplicationFromApplicant(applicantId, applicationDto)).build();
	}
}
