package kiwi.itech.service;

import java.util.List;

import javax.ejb.Local;

import kiwi.itech.kiwibackend.dtos.ChildDto;

@Local
public interface IChildService {


	ChildDto  saveChild(ChildDto child);
	ChildDto  updateChild(String childId, ChildDto child);
	ChildDto  findChild(String childId);
	List<ChildDto>  findChilds();
	boolean  deleteChild(String childId);
	ChildDto findByTaxNo(String taxNo);
}
