package kiwi.itech.kiwibackend.dtos;

import java.time.LocalDate;
import java.util.List;
import java.util.StringJoiner;

import kiwi.itech.model.entities.Partner;
import kiwi.itech.model.entities.PhoneNumber;
import kiwi.itech.model.entities.enums.GenderType;

public class PartnerDto extends PersonDto{

	public PartnerDto() {}

	@Override
	public String getFirstname() {
		return super.getFirstname();
	}

	@Override
	public PartnerDto setFirstname(String firstname) {
		 super.setFirstname(firstname);
		 return this;
	}

	@Override
	public String getLastname() {
		return super.getLastname();
	}

	@Override
	public PartnerDto setLastname(String lastname) {
		 super.setLastname(lastname);
		 return this;
	}

	@Override
	public String getBirthName() {
		return super.getBirthName();
	}

	@Override
	public PartnerDto setBirthName(String birthName) {
		 super.setBirthName(birthName);
		 return this;
	}

	@Override
	public String getEmail() {
		return super.getEmail();
	}

	@Override
	public PartnerDto setEmail(String email) {
		 super.setEmail(email);
		 return this;
	}

	@Override
	public GenderType getGender() {
		return super.getGender();
	}

	@Override
	public PartnerDto setGender(GenderType gender) {
		 super.setGender(gender);
		 return this;
	}

	@Override
	public LocalDate getDateOfBirth() {
		return super.getDateOfBirth();
	}

	@Override
	public PartnerDto setDateOfBirth(LocalDate dateOfBirth) {
		 super.setDateOfBirth(dateOfBirth);
		 return this;
	}

	@Override
	public String getTaxNo() {
		return super.getTaxNo();
	}

	@Override
	public PartnerDto setTaxNo(String taxNo) {
		 super.setTaxNo(taxNo);
		 return this;
	}

	@Override
	public String getNationality() {
		return super.getNationality();
	}

	@Override
	public PartnerDto setNationality(String nationality) {
		 super.setNationality(nationality);
		 return this;
	}

	@Override
	public String getLocalOfBirth() {
		return super.getLocalOfBirth();
	}

	@Override
	public PartnerDto setLocalOfBirth(String localOfBirth) {
		 super.setLocalOfBirth(localOfBirth);
		 return this;
	}

	@Override
	public String getAdditionAddress() {
		return super.getAdditionAddress();
	}

	@Override
	public PartnerDto setAdditionAddress(String additionAddress) {
		super.setAdditionAddress(additionAddress);
		return this;
	}

	@Override
	public AddressDto getAddress() {
		return super.getAddress();
	}

	@Override
	public PartnerDto setAddress(AddressDto address) {
		super.setAddress(address);
		return this;
	}

	@Override
	public List<PhoneNumber> getPhones() {
		return super.getPhones();
	}

	@Override
	public PartnerDto setPhones(List<PhoneNumber> phones) {
		super.setPhones(phones);
		return this;
	}
	
	@Override
	public String getPersonId() {
		return super.getPersonId();
	}

	@Override
	public PartnerDto setPersonId(String personId) {
		super.setPersonId(personId);
		return this;
	}

	public Partner toPojo() {
		Partner partner = new Partner();
		partner.setPersonId(getPersonId());
		partner.setFirstname(getFirstname());
		partner.setLastname(getLastname());
		partner.setEmail(getEmail());
		partner.setGender(getGender());
		partner.setDateOfBirth(getDateOfBirth());
		partner.setTaxNo(getTaxNo());
		partner.setNationality(getNationality());
		partner.setLocalOfBirth(getLocalOfBirth());
		partner.setAdditionAddress(getAdditionAddress());
		partner.setPhones(getPhones());

		if(getAddress() == null || getAddress().isNull()) {
			partner.setAddress(null);
		}else {
			partner.setAddress(getAddress().toPojo());
		}
		return partner;
	}

	public boolean isNull() {
		if((this.getEmail() == null|| this.getEmail().equals(""))
				&& (this.getTaxNo() == null || this.getTaxNo().equals(""))
				&& (this.getFirstname() == null || this.getFirstname().equals(""))
				&& (this.getLastname() == null || this.getLastname().equals("")))
			return true;
		return false;
	}

}
