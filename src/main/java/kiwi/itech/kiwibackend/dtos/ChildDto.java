package kiwi.itech.kiwibackend.dtos;

import java.time.LocalDate;
import java.util.List;

import kiwi.itech.model.entities.Child;
import kiwi.itech.model.entities.PhoneNumber;
import kiwi.itech.model.entities.enums.GenderType;
import kiwi.itech.model.entities.enums.RelationShipType;

public class ChildDto extends PersonDto {

	private PartnerDto partner;
	private RelationShipType relationWithPartner;
	private RelationShipType relationWithApplicant;

	public ChildDto() {
	} 

	public PartnerDto getPartner() {
		return partner;
	}

	public ChildDto setPartner(PartnerDto partner) {
		this.partner = partner;
		return this;
	}

	public RelationShipType getRelationWithPartner() {
		return relationWithPartner;
	}

	public ChildDto setRelationWithPartner(RelationShipType relationWithPartner) {
		this.relationWithPartner = relationWithPartner;
		return this;
	}

	public RelationShipType getRelationWithApplicant() {
		return relationWithApplicant;
	}

	public ChildDto setRelationWithApplicant(RelationShipType relationWithApplicant) {
		this.relationWithApplicant = relationWithApplicant;
		return this;
	}

	@Override
	public String getFirstname() {
		return super.getFirstname();
	}

	@Override
	public ChildDto setFirstname(String firstname) {
		super.setFirstname(firstname);
		return this;
	}

	@Override
	public String getLastname() {
		return super.getLastname();
	}

	@Override
	public ChildDto setLastname(String lastname) {
		super.setLastname(lastname);
		return this;
	}

	@Override
	public String getBirthName() {
		return super.getBirthName();
	}

	@Override
	public ChildDto setBirthName(String birthName) {
		super.setBirthName(birthName);
		return this;
	}

	@Override
	public String getEmail() {
		return super.getEmail();
	}

	@Override
	public ChildDto setEmail(String email) {
		super.setEmail(email);
		return this;
	}

	@Override
	public GenderType getGender() {
		return super.getGender();
	}

	@Override
	public ChildDto setGender(GenderType gender) {
		super.setGender(gender);
		return this;
	}

	@Override
	public LocalDate getDateOfBirth() {
		return super.getDateOfBirth();
	}

	@Override
	public ChildDto setDateOfBirth(LocalDate dateOfBirth) {
		super.setDateOfBirth(dateOfBirth);
		return this;
	}

	@Override
	public String getTaxNo() {
		return super.getTaxNo();
	}

	@Override
	public ChildDto setTaxNo(String taxNo) {
		super.setTaxNo(taxNo);
		return this;
	}

	@Override
	public String getNationality() {
		return super.getNationality();
	}

	@Override
	public ChildDto setNationality(String nationality) {
		super.setNationality(nationality);
		return this;
	}

	@Override
	public String getLocalOfBirth() {
		return super.getLocalOfBirth();
	}

	@Override
	public ChildDto setLocalOfBirth(String localOfBirth) {
		super.setLocalOfBirth(localOfBirth);
		return this;
	}

	@Override
	public String getAdditionAddress() {
		return super.getAdditionAddress();
	}

	@Override
	public ChildDto setAdditionAddress(String additionAddress) {
		super.setAdditionAddress(additionAddress);
		return this;
	}

	@Override
	public AddressDto getAddress() {
		return super.getAddress();
	}

	@Override
	public ChildDto setAddress(AddressDto address) {
		super.setAddress(address);
		return this;
	}

	@Override
	public List<PhoneNumber> getPhones() {
		return super.getPhones();
	}

	@Override
	public ChildDto setPhones(List<PhoneNumber> phones) {
		super.setPhones(phones);
		return this;
	}

	@Override
	public String getPersonId() {
		return super.getPersonId();
	}

	@Override
	public ChildDto setPersonId(String personId) {
		super.setPersonId(personId);
		return this;
	}

	public Child toPojo() {
		Child child = new Child();
		child.setPersonId(getPersonId());
		child.setFirstname(getFirstname());
		child.setLastname(getLastname());
		child.setEmail(getEmail());
		child.setGender(getGender());
		child.setDateOfBirth(getDateOfBirth());
		child.setTaxNo(getTaxNo());
		child.setNationality(getNationality());
		child.setLocalOfBirth(getLocalOfBirth());
		child.setAdditionAddress(getAdditionAddress());
		child.setPhones(getPhones());
		child.setRelationWithApplicant(getRelationWithApplicant());
		child.setRelationWithPartner(getRelationWithPartner());

		if(getAddress() == null || getAddress().isNull()) {
			child.setAddress(null);
		}else {
			child.setAddress(getAddress().toPojo());
		}
		return child;
	}
}
