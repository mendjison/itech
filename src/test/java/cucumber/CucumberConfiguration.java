package cucumber;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.Before;
import io.cucumber.java.DefaultDataTableCellTransformer;
import io.cucumber.java.DefaultDataTableEntryTransformer;
import io.cucumber.java.DefaultParameterTransformer;
import com.fasterxml.jackson.datatype.jsr310.*;

import java.lang.reflect.Type;
public class CucumberConfiguration {
    private  ObjectMapper objectMapper;

    public CucumberConfiguration() {
        objectMapper = new ObjectMapper();
        //objectMapper.registerModule(new JavaTimeModule());
    }

    //@DefaultDataTableCellTransformer
    //@DefaultDataTableEntryTransformer
    //@DefaultParameterTransformer
    public Object transform(final Object  from, final Type to) {
        return  objectMapper.convertValue(from, objectMapper.constructType(to));
    }
}
