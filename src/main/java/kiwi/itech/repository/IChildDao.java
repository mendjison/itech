package kiwi.itech.repository;

import kiwi.itech.model.entities.Child;
import kiwi.itech.repository.generic.JpaRepository;

import javax.ejb.Local;
import java.util.Optional;

@Local
public interface IChildDao extends JpaRepository<Child, String> {

    Optional<Child> findByTaxNo(String taxNo);
}
