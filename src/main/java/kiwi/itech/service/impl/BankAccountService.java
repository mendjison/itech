package kiwi.itech.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import kiwi.itech.model.entities.BankAccount;
import kiwi.itech.model.helper.exception.ExceptionMessage;
import kiwi.itech.model.helper.exception.NotNullException;
import kiwi.itech.repository.IBankAccountDao;
import kiwi.itech.service.IBankAccountService;
import kiwi.itech.kiwibackend.dtos.BankAccountDto;

@Stateless(name = "BankAccountService")
public class BankAccountService implements IBankAccountService{

	@EJB(name = "BankAccountDao")
	private IBankAccountDao bankAccountDao;
	
	public BankAccountService() {}

	@Override
	public BankAccountDto findBankAccountByIban(String iban) {
	
		BankAccount bankAccount = bankAccountDao.findByIban(iban).orElse(null);
		if(bankAccount == null)
			return null;
		return bankAccount.toDto();
	}

	@Override
	public BankAccountDto saveBankAccount(BankAccountDto accountDto) {
		
		validationBankAccount(accountDto);

		accountDto.setId(UUID.randomUUID().toString().substring(0,6));

		BankAccount bankAccount = bankAccountDao.save(accountDto.toPojo());
		if(bankAccount == null)
			return null;
		return bankAccount.toDto();
	}

	@Override
	public BankAccountDto updateBankAccount(String id, BankAccountDto accountDto) {
		Optional<BankAccount> bankAccount = bankAccountDao.findById(id);
		if(bankAccount.isPresent())
		{
			accountDto.setId(bankAccount.get().getId());
			return bankAccountDao.update(accountDto.toPojo()).toDto();
		}else {
			return saveBankAccount(accountDto);
		}
	}

	@Override
	public boolean deleteBankAccountById(String id) {
		Optional<BankAccount> bankAccount = bankAccountDao.findById(id);
		if(bankAccount.isPresent())
		{
			return bankAccountDao.delete(bankAccount.get());
		}else {
			return false;
		}
	}

	@Override
	public List<BankAccountDto> findBankAccounts() {
		return bankAccountDao.findAll()
				.stream()
				.map((BankAccount account) -> account.toDto())
				.collect(Collectors.toList());
	}

	@Override
	public BankAccountDto findBankAccountById(String id) {
		
		BankAccount bankAccount = bankAccountDao.findById(id).orElse(null);
		if(bankAccount == null)
			return null;
		return bankAccount.toDto();
	}
	
    public void validationBankAccount(BankAccountDto account) {
    	
    	if(account == null)
    		throw new NotNullException(ExceptionMessage.BANKACCOUNT_MUST_NOT_NULL);
    	
    	if(account.getIban() == null || account.getIban().equals(""))
    		throw new NotNullException(ExceptionMessage.IBAN_MUST_NOT_NULL);
    	
    	if(findBankAccountByIban(account.getIban()) != null) 
    		throw new NotNullException(ExceptionMessage.IBAN_ALREADY_EXISTS);
    }

}
