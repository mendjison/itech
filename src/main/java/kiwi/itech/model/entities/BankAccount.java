package kiwi.itech.model.entities;

import kiwi.itech.kiwibackend.dtos.BankAccountDto;

import java.io.Serializable;
import java.util.Objects;
import java.util.StringJoiner;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery(name = "BankAccount.findByIban", query = "select account from BankAccount as account where account.iban = :iban"),
	@NamedQuery(name = "BankAccount.findBankAccounts", query = "select account from BankAccount as account")
})
public class BankAccount  implements Serializable{

	@Id
	@Column(length = 7)
	private String id;
	@Column(length = 22, unique = true, nullable = false)
	private String iban; 
	@Column(length = 30, nullable = false)
	private String bic;
	private String bankName;
	@Embedded
	private BankAccountOwner accountOwner;
	public BankAccount() {}
	public BankAccount(String iban, String bic, String bankName, BankAccountOwner accountOwner) {
		super();
		this.iban = iban;
		this.bic = bic;
		this.bankName =bankName;
		this.accountOwner = accountOwner;
	}
	
	public BankAccount(String iban, String bic) {
		super();
		this.iban = iban;
		this.bic = bic;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIban() {
		return iban;
	}
	public void setIban(String iban) {
		this.iban = iban;
	}
	public String getBic() {
		return bic;
	}
	public void setBic(String bic) {
		this.bic = bic;
	}
	public BankAccountOwner getAccountOwner() {
		return accountOwner;
	}
	public void setAccountOwner(BankAccountOwner accountOwner) {
		this.accountOwner = accountOwner;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", BankAccount.class.getSimpleName() + "[", "]")
				.add("id='" + id + "'")
				.add("iban='" + iban + "'")
				.add("bic='" + bic + "'")
				.add("bankName='" + bankName + "'")
				.add("accountOwner=" + accountOwner)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof BankAccount)) return false;
		BankAccount that = (BankAccount) o;
		return getId().equals(that.getId()) &&
				getIban().equals(that.getIban()) &&
				getBic().equals(that.getBic()) &&
				getBankName().equals(that.getBankName()) &&
				getAccountOwner().equals(that.getAccountOwner());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId(), getIban(), getBic(), getBankName(), getAccountOwner());
	}

	public BankAccountDto toDto() {
		return new BankAccountDto()
				.setId(getId())
				.setBic(getBic())
				.setIban(getIban())
				.setBankName(getBankName())
				.setAccountOwner(getAccountOwner());
	}
}
