package kiwi.itech.repository.daoImpl;

import kiwi.itech.model.entities.Partner;
import kiwi.itech.repository.IPartnerDao;
import kiwi.itech.repository.generic.JpaRepositoryImpl;

import javax.ejb.Stateless;
import java.util.List;
import java.util.Optional;

@Stateless(name="PartnerDao")
public class PartnerDao extends JpaRepositoryImpl<Partner, String> implements IPartnerDao {

    @Override
    public Optional<Partner> findPartnerByEmail(String email) {
        List<Partner> partners = getEntityManager()
                .createNamedQuery("Partner.findPartnerByEmail", Partner.class)
                .setParameter("email", email).getResultList();

        return (partners == null || partners.isEmpty()) ? Optional.empty() : Optional.of(partners.get(0));
    }

    @Override
    public Optional<Partner> findByTaxNo(String taxNo) {
        List<Partner> partners = getEntityManager()
                .createNamedQuery("Partner.findChildByTaxNo", Partner.class)
                .setParameter("taxNo", taxNo).getResultList();

        return (partners == null || partners.isEmpty()) ? Optional.empty():Optional.of(partners.get(0));
    }
}
