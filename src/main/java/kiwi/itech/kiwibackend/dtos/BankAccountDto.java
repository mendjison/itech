package kiwi.itech.kiwibackend.dtos;

import kiwi.itech.model.entities.BankAccount;
import kiwi.itech.model.entities.BankAccountOwner;

public class BankAccountDto {

	private String iban; 
	private String bic;
	private BankAccountOwner accountOwner;
	private String bankName;
	private String id;

	public BankAccountDto() {}
	
	public String getId() {
		return id;
	}

	public BankAccountDto setId(String id) {
		this.id = id;
		return this;
	}

	public String getIban() {
		return iban;
	}

	public BankAccountDto setIban(String iban) {
		this.iban = iban;
		return this;
	}

	public String getBic() {
		return bic;
	}

	public BankAccountDto setBic(String bic) {
		this.bic = bic;
		return this;
	}

	public BankAccountOwner getAccountOwner() {
		return accountOwner;
	}

	public BankAccountDto setAccountOwner(BankAccountOwner accountOwner) {
		this.accountOwner = accountOwner;
		return this;
	}

	public String getBankName() {
		return bankName;
	}

	public BankAccountDto setBankName(String bankName) {
		this.bankName = bankName;
		return this;
	}

	public BankAccount toPojo() {
		BankAccount bankAccount = new BankAccount();
		bankAccount.setId(getId());
		bankAccount.setBankName(getBankName());
		bankAccount.setBic(getBic());
		bankAccount.setIban(getIban());
		bankAccount.setAccountOwner(getAccountOwner());
		return bankAccount;
	}
}
