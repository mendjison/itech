package cucumber;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.Before;
import io.cucumber.java.DefaultDataTableCellTransformer;
import io.cucumber.java.DefaultDataTableEntryTransformer;
import io.cucumber.java.DefaultParameterTransformer;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;

import java.io.File;
import java.lang.reflect.Type;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "json:target/cucumber.json"},
        features = "classpath:features",glue = "cucumber.steps")
public class CucumberIT {
    private ObjectMapper objectMapper;

    @Deployment(testable = true, name = "bankAccount")
    public static WebArchive createDeployment() {

         File archiveFile = new File("target/backendKiwiKino.war");
         return ShrinkWrap.createFromZipFile(WebArchive.class, archiveFile)
         .addAsResource(new File("src/test/resources/META-INF/test-persistence.xml"),
          "META-INF/persistence.xml") .addAsManifestResource(EmptyAsset.INSTANCE,
         "beans.xml");

    }


    @DefaultDataTableCellTransformer
    @DefaultDataTableEntryTransformer
    @DefaultParameterTransformer
    @Before
    public Object transform(final Object  from, final Type to) {
        objectMapper = new ObjectMapper();
        return  objectMapper.convertValue(from, objectMapper.constructType(to));
    }
}
