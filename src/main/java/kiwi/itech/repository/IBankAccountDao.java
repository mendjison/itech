package kiwi.itech.repository;

import kiwi.itech.model.entities.BankAccount;
import kiwi.itech.repository.generic.JpaRepository;

import javax.ejb.Local;
import java.util.Optional;

@Local
public interface IBankAccountDao extends JpaRepository<BankAccount, String> {
    Optional<BankAccount> findByIban(String iban);
}
