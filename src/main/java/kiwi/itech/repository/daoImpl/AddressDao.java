package kiwi.itech.repository.daoImpl;

import kiwi.itech.model.entities.Address;
import kiwi.itech.repository.IAddressDao;
import kiwi.itech.repository.generic.JpaRepositoryImpl;

import javax.ejb.Stateless;
import java.util.List;

@Stateless(name = "AddressDao")
public class AddressDao extends JpaRepositoryImpl<Address, String> implements IAddressDao {

    @Override
    public Address findAddress(String street, String houseNo, int postcode, String country) {
        List<Address> addresses = getEntityManager().createNamedQuery("Address.findByStreetAndHouseNoAndByPostcodeAndCountry", Address.class)
                .setParameter("street", street)
                .setParameter("houseNo", houseNo)
                .setParameter("postcode", postcode)
                .setParameter("country", country)
                .getResultList();
        return (addresses == null || addresses.isEmpty()) ? null: addresses.get(0);
    }
}
