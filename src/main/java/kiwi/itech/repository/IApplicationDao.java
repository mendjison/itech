package kiwi.itech.repository;

import kiwi.itech.model.entities.Applicant;
import kiwi.itech.model.entities.Application;
import kiwi.itech.repository.generic.JpaRepository;

import javax.ejb.Local;
import java.util.List;

@Local
public interface IApplicationDao extends JpaRepository<Application, String> {
    List<Application> findByApplicant(Applicant applicant);
}
