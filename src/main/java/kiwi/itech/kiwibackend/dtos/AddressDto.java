package kiwi.itech.kiwibackend.dtos;

import kiwi.itech.model.entities.Address;

public class AddressDto {

	private String street;
	private String houseNo;
	private int postcode;
	private String city;
	private String country;
	private String addressId;
	public AddressDto() {}
	
	public AddressDto(String street, String houseNo, int postcode, String city, String country) {
		this.street = street;
		this.houseNo = houseNo;
		this.postcode = postcode;
		this.city = city;
		this.country = country;
	}

	public String getStreet() {
		return street;
	}
	public AddressDto setStreet(String street) {
		this.street = street;
		return this;
	}
	public String getHouseNo() {
		return houseNo;
	}
	public AddressDto setHouseNo(String houseNo) {
		this.houseNo = houseNo;
		return this;
	}
	public int getPostcode() {
		return postcode;
	}
	public AddressDto setPostcode(int postcode) {
		this.postcode = postcode;
		return this;
	}
	public String getCity() {
		return city;
	}
	public AddressDto setCity(String city) {
		this.city = city;
		return this;
	}
	public String getCountry() {
		return country;
	}
	public AddressDto setCountry(String country) {
		this.country = country;
		return this;
	}

	public String getAddressId() {
		return addressId;
	}

	public AddressDto setAddressId(String addressId) {
		this.addressId = addressId;
		return this;
	}

	public Address toPojo() {
		Address address = new Address();
		address.setAddressId(getAddressId());
		address.setStreet(getStreet());
		address.setHouseNo(getHouseNo());
		address.setPostcode(getPostcode());
		address.setCity(getCity());
		address.setCountry(getCountry());
		return address;
	}

	public boolean isNull() {
		if((this.getStreet() == null || this.getStreet().equals(""))
				&& (this.getHouseNo() == null || this.getStreet().equals(""))
				&& this.getPostcode() == 0)
			return true;
		return false;
	}
	
}
