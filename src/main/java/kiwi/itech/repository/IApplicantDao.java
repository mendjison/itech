package kiwi.itech.repository;

import kiwi.itech.kiwibackend.dtos.SearchAttribute;
import kiwi.itech.model.entities.Address;
import kiwi.itech.model.entities.Applicant;
import kiwi.itech.repository.generic.JpaRepository;

import javax.ejb.Local;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Local
public interface IApplicantDao extends JpaRepository<Applicant, String> {
    Optional<Applicant> findByUsername(String username);

    Optional<Applicant> findByChildReferenceNo(String childReferenceNo);

    Optional<Applicant> findByTaxNo(String taxNo);

    Optional<Applicant> findByEmail(String email);

    List<Applicant> findByAddress(Address address);

    List<Applicant> findByDateOfBirth(LocalDate dateOfBirth);

    List<Applicant> findByFirtsnameAndLastname(String firstname, String lastname);

    List<Applicant> searchApplicants(SearchAttribute searchAttribute);

}
