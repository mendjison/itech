package kiwi.itech.model.entities;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import kiwi.itech.kiwibackend.dtos.AddressDto;
import kiwi.itech.model.entities.enums.RelationShipType;
import kiwi.itech.kiwibackend.dtos.ChildDto;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
	@NamedQuery(name = "Child.findChildById", query = "select user from Child as user where user.personId = :personId"),
	@NamedQuery(name = "Child.findChildByEmail", query = "select user from Child as user where user.email = :email"),
	@NamedQuery(name = "Child.findChildByTaxNo", query = "select user from Child as user where user.taxNo = :taxNo"),
	@NamedQuery(name = "Child.findChilds", query = "select user from Child as user")
})
public class Child extends Person  {

	@Enumerated(EnumType.STRING)
	private RelationShipType relationWithPartner;
	@Enumerated(EnumType.STRING)
	private RelationShipType relationWithApplicant;
	public Child() {super();}

	public RelationShipType getRelationWithPartner() {
		return relationWithPartner;
	}

	public void setRelationWithPartner(RelationShipType relationWithPartner) {
		this.relationWithPartner = relationWithPartner;
	}

	public RelationShipType getRelationWithApplicant() {
		return relationWithApplicant;
	}

	public void setRelationWithApplicant(RelationShipType relationWithApplicant) {
		this.relationWithApplicant = relationWithApplicant;
	}
	

	@Override
	public String toString() {
		return "Child [relationWithPartner=" + relationWithPartner + ", relationWithApplicant="
				+ relationWithApplicant + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((relationWithApplicant == null) ? 0 : relationWithApplicant.hashCode());
		result = prime * result + ((relationWithPartner == null) ? 0 : relationWithPartner.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Child other = (Child) obj;
		if (relationWithApplicant != other.relationWithApplicant)
			return false;
		if (relationWithPartner != other.relationWithPartner)
			return false;
		return true;
	}

	public ChildDto toDto() {
		return new ChildDto()
				.setPersonId(getPersonId())
				.setFirstname(getFirstname())
				.setLastname(getLastname())
				.setEmail(getEmail())
				.setTaxNo(getTaxNo())
				.setBirthName(getBirthName())
				.setNationality(getNationality())
				.setLocalOfBirth(getLocalOfBirth())
				.setDateOfBirth(getDateOfBirth())
				.setAdditionAddress(getAdditionAddress())
				.setAddress((getAddress() == null || getAddress().isNull())? new AddressDto() : getAddress().toDto())
				.setPhones(getPhones())
				.setGender(getGender())
				.setRelationWithApplicant(getRelationWithApplicant())
				.setRelationWithPartner(getRelationWithPartner());
	}
}
