package kiwi.itech.repository.generic;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
public interface JpaRepository<T extends Serializable, ID > {
    T save(T entity);
    T update(T entity);
    Optional<T> findById(ID id);
    List<T> findAll();
    boolean delete(T entity);
}