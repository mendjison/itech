package kiwi.itech.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import kiwi.itech.kiwibackend.dtos.*;
import kiwi.itech.model.entities.Application;
import kiwi.itech.model.entities.enums.StatusType;
import kiwi.itech.model.helper.exception.ExceptionMessage;
import kiwi.itech.model.helper.exception.NotNullException;
import kiwi.itech.repository.IApplicationDao;
import kiwi.itech.service.*;

@Stateless(name = "ApplicationService")
public class ApplicationService implements IApplicationService {

	@EJB(name = "ApplicationDao")
	private IApplicationDao applicationDao;
	@EJB(name = "ApplicantService")
	private IApplicantService applicantService;

	@EJB(name = "BankAccountService")
	private IBankAccountService accountService;

	@EJB(name = "ChildService")
	private IChildService childService;
	@EJB(name = "PartnerService")
	private IPartnerService partnerService;

	public ApplicationService() {
	}

	@Override
	public ApplicationDto createApplication(ApplicationDto applicationDto) {

		validationApplication(applicationDto);

		applicationDto.setStatus(StatusType.CREATE);

		editOrSaveBankAccount(applicationDto);

		editOrSaveApplicant(applicationDto);

		editOrSavePartner(applicationDto);

		editOrSaveChilds(applicationDto);

		applicationDto.setId(UUID.randomUUID().toString().substring(0,6));

		return applicationDao.save(applicationDto.toPojo()).toDto();

	}

	@Override
	public ApplicationDto updateApplication(String applicationId, ApplicationDto applicationDto) {

		validationApplication(applicationDto);

		editOrSaveBankAccount(applicationDto);

		editOrSaveApplicant(applicationDto);

		editOrSaveChilds(applicationDto);

		editOrSavePartner(applicationDto);

		Application application = applicationDto.toPojo();
		application.setStatus(StatusType.INWORK);

		Application applicationOlt = applicationDao.findById(applicationId).orElse(null);
		if(applicationOlt == null)
			return createApplication(applicationDto);

		application.setId(applicationOlt.getId());
		return applicationDao.update(application).toDto();
	}

	@Override
	public ApplicationDto createApplicationFromApplicant(String applicantId, ApplicationDto applicationDto) {
		
		ApplicantDto applicantDto = applicantService.findApplicant(applicantId);
		
		applicationDto.setApplicant(applicantDto);

		validationApplication(applicationDto);

		applicationDto.setStatus(StatusType.CREATE);

		editOrSaveBankAccount(applicationDto);

		editOrSaveApplicantFromApplicant(applicationDto);

		editOrSaveChilds(applicationDto);

		applicationDto.setId(UUID.randomUUID().toString().substring(0,6));
		return applicationDao.save(applicationDto.toPojo()).toDto();
	}

	@Override
	public ApplicationDto updateApplicationFromApplicant(String applicationId, ApplicationDto applicationDto) {

		validationApplication(applicationDto);

		editOrSaveBankAccount(applicationDto);

		editOrSaveApplicantFromApplicant(applicationDto);

		editOrSaveChilds(applicationDto);

		Application application = applicationDto.toPojo();
		application.setStatus(StatusType.INWORK);

		Application applicationOlt = applicationDao.findById(applicationId).orElse(null);
		if(applicationOlt == null)
			return createApplication(applicationDto);

		application.setId(applicationOlt.getId());
		return applicationDao.update(application).toDto();
	}

	@Override
	public boolean deleteApplication(String applicationId) {
		Application application = applicationDao.findById(applicationId).orElse(null);
		if(application == null)
			return false;
		return applicationDao.delete(application);
	}

	@Override
	public List<ApplicationDto> getApplications() {

		return applicationDao.findAll().stream()
				.map((Application application) -> application.toDto())
				.collect(Collectors.toList());	
	}
	
	@Override
	public ApplicationDto accepteApplication(String applicationId) {
		Application application = applicationDao.findById(applicationId).orElse(null);
		if(application == null)
			return null;
		application.setStatus(StatusType.ACCEPT);
		return applicationDao.update(application).toDto();
	}

	@Override
	public ApplicationDto rejectApplication(String applicationId) {
		Application application = applicationDao.findById(applicationId).orElse(null);
		if(application == null)
			return null;
		application.setStatus(StatusType.REJECT);
		return applicationDao.update(application).toDto();
	}
	
	@Override
	public List<ApplicationDto> getApplicationsByApplicant(String applicationId) {
		ApplicantDto applicantDto = applicantService.findApplicant(applicationId);
		if(applicantDto == null) 
			return new ArrayList<ApplicationDto>();
		
		return applicationDao.findByApplicant(applicantDto.toPojo())
				.stream()
				.map((Application application) -> application.toDto())
				.collect(Collectors.toList());
	}
	
	@Override
	public ApplicationDto findApplication(String applicationId) {
		Application application =
				applicationDao.findById(applicationId).orElse(null);
		if(application == null)
			return null;
		return application.toDto();
	}

	/*
	 * @Override public List<ApplicationDto>
	 * getApplicationsByChildReferenceNo(String childReferenceNo) {
	 * 
	 * ApplicantDto applicantDto =
	 * applicantService.findByChildReferenceNo(childReferenceNo);
	 * 
	 * if (applicantDto == null) return null;
	 * 
	 * return applicationRepository.findApplicant(convertDtoToPojoAndPojoToDto.
	 * createApplicant(applicantDto)).stream() .map((Application application) ->
	 * convertDtoToPojoAndPojoToDto.createApplicationDto(application))
	 * .collect(Collectors.toList()); }
	 * 
	 * @Override public List<ApplicationDto> getApplicationsByEmail(String email) {
	 * 
	 * ApplicantDto applicantDto = applicantService.findByEmail(email);
	 * 
	 * if (applicantDto == null) return null;
	 * 
	 * return applicationRepository.findApplicant(convertDtoToPojoAndPojoToDto.
	 * createApplicant(applicantDto)).stream() .map((Application application) ->
	 * convertDtoToPojoAndPojoToDto.createApplicationDto(application))
	 * .collect(Collectors.toList()); }
	 * 
	 * @Override public List<ApplicationDto>
	 * getApplicationsByFirstnameAndDateOfBirth(String firstname, LocalDate
	 * dateOfBirth) { // TODO Auto-generated method stub return null; }
	 * 
	 * @Override public Optional<ApplicationDto> getByTaxNo(String taxNo) { // TODO
	 * Auto-generated method stub return null; }
	 * 
	 * @Override public List<ApplicationDto> getByAddress(Address address) { // TODO
	 * Auto-generated method stub return null; }
	 * 
	 * @Override public List<ApplicationDto> getByDateOfBirth(LocalDate dateOfBirth)
	 * { // TODO Auto-generated method stub return null; }
	 * 
	 * @Override public List<ApplicationDto> getByFirtsnameAndLastname(String
	 * firstname, String lastname) { // TODO Auto-generated method stub return null;
	 * }
	 * 
	 * @Override public List<ApplicationDto>
	 * getByUsernameOrChildReferenceNoOrByTaxNoOrEmailOrAddressOrDateOfBirth(String
	 * username, String childReferenceNo, String taxNo, String email, Address
	 * address, String dateOfBirth) { // TODO Auto-generated method stub return
	 * null; }
	 */

	private void validationApplication(ApplicationDto applicationDto) {

		if (applicationDto == null)
			throw new NotNullException(ExceptionMessage.APPLICATION_MUST_NOT_NULL);

		if (applicationDto.getAccount() == null)
			throw new NotNullException(ExceptionMessage.BANKACCOUNT_MUST_NOT_NULL);

		if (applicationDto.getApplicant() == null)
			throw new NotNullException(ExceptionMessage.APPLICANT_MUST_NOT_NULL);

		if (applicationDto.getChilds() == null || applicationDto.getChilds().isEmpty())
			throw new NotNullException(ExceptionMessage.CHILD_MUST_NOT_NULL);

		if (applicationDto.getApplicationDate() == null)
			applicationDto.setApplicationDate(LocalDate.now());
	}

	private void editOrSaveApplicant(ApplicationDto applicationDto) {

		ApplicantDto applicantDto = applicantService.findByTaxNo(applicationDto.getApplicant().getTaxNo());

		if (applicantDto == null)
			applicantDto = applicantService.findByEmail(applicationDto.getApplicant().getEmail());

		if (applicantDto == null)
			applicantDto = applicantService.findByChildReferenceNo(applicationDto.getApplicant().getChildReferenceNo());

		if (applicantDto == null) {
			applicantDto = applicantService.saveApplicant(applicationDto.getApplicant());
			applicationDto.setApplicant(applicantDto);
		} else {
			applicationDto.getApplicant().setPersonId(applicantDto.getPersonId());
		}
	}

	private void editOrSaveApplicantFromApplicant(ApplicationDto applicationDto) {

		ApplicantDto applicantDto = applicantService.findByTaxNo(applicationDto.getApplicant().getTaxNo());

		if (applicantDto == null)
			applicantDto = applicantService.findByEmail(applicationDto.getApplicant().getEmail());

		if (applicantDto == null)
			applicantDto = applicantService.findByChildReferenceNo(applicationDto.getApplicant().getChildReferenceNo());

		if (applicantDto == null) {
			applicantDto = applicantService.saveApplicantfromApplicant(applicationDto.getApplicant());
			applicationDto.setApplicant(applicantDto);
		} else {
			applicationDto.getApplicant().setPersonId(applicantDto.getPersonId());
		}
	}

	private void editOrSaveChilds(ApplicationDto applicationDto) {

		List<ChildDto> childDtos = new ArrayList<ChildDto>();

		for (ChildDto childDto : applicationDto.getChilds()) {
			childDtos.add(editOrSaveChild(childDto));
		}

		applicationDto.setChilds(childDtos);
	}

	private ChildDto editOrSaveChild(ChildDto childDto) {

		ChildDto findChildDto = childService.findByTaxNo(childDto.getTaxNo());
		if (findChildDto == null) {
			childDto = childService.saveChild(childDto);
		} else {
			childDto.setPersonId(findChildDto.getPersonId());
		}

		return childDto;
	}

	private void editOrSaveBankAccount(ApplicationDto applicationDto) {
		BankAccountDto accountDto = accountService.findBankAccountByIban(applicationDto.getAccount().getIban());
		if (accountDto == null) {
			accountDto = accountService.saveBankAccount(applicationDto.getAccount());
			applicationDto.setAccount(accountDto);
		} else {
			applicationDto.getAccount().setId(accountDto.getId());
		}
	}

	private void editOrSavePartner(ApplicationDto applicationDto)  {

		if(applicationDto.getPartner() == null || applicationDto.getPartner().isNull())
		{
			applicationDto.setPartner(null);
			return;
		}

		PartnerDto partnerDto = partnerService.findByTaxNo(applicationDto.getPartner().getTaxNo());

		if(partnerDto == null)
			partnerDto = partnerService.findPartnerByEmail(applicationDto.getPartner().getEmail());

		if (partnerDto == null) {
			partnerDto = partnerService.savePartner(applicationDto.getPartner());
			applicationDto.setPartner(partnerDto);
		} else {
			applicationDto.getPartner().setPersonId(partnerDto.getPersonId());
		}
	}
}
