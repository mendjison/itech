package kiwi.itech.service;

import java.time.LocalDate;
import java.util.List;


import javax.ejb.Local;

import kiwi.itech.kiwibackend.dtos.SearchAttribute;
import kiwi.itech.model.entities.Address;
import kiwi.itech.kiwibackend.dtos.ApplicantDto;


@Local
public interface IApplicantService {

	ApplicantDto saveApplicant(ApplicantDto applicantDto);
	ApplicantDto updateApplicant(String id, ApplicantDto applicantDto);
	List<ApplicantDto> findApplicants();
	ApplicantDto findApplicant(String personId);
	boolean deleteApplicant(String personId);
	ApplicantDto findByTaxNo(String taxNo);
	ApplicantDto findByEmail(String email);
	ApplicantDto findByChildReferenceNo(String childReferenceNo);
	
	ApplicantDto saveApplicantfromApplicant(ApplicantDto applicantDto);
	
	List<ApplicantDto> findByAddress(Address address);
	List<ApplicantDto> findByDateOfBirth(LocalDate dateOfBirth);
	List<ApplicantDto> findByFirtsnameAndLastname(String firstname, String lastname);
	List<ApplicantDto> searchApplicants(SearchAttribute searchAttribute);
}
