package kiwi.itech.model.helper.exception;

@SuppressWarnings("serial")
public class NotNullException extends RuntimeException{

	public NotNullException(String message) {
		super(message);
	}

}
