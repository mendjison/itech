package unittest.service.bankAccount;

import kiwi.itech.model.entities.BankAccount;
import kiwi.itech.repository.IBankAccountDao;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import kiwi.itech.service.impl.BankAccountService;

import java.util.Optional;
import java.util.UUID;

@RunWith(MockitoJUnitRunner.class)
public class DeleteBankAccountByIdUnitTest {

	@Mock
    private IBankAccountDao iBankAccountRepository;
	
    @InjectMocks
    private BankAccountService bankAccountService;

    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }
    
    @Test
    public void whenDeleted() {
    	
        //given
    	String id = UUID.randomUUID().toString();
        BankAccount bankAccount = new BankAccount();
        bankAccount.setId(id);

        Mockito.when(iBankAccountRepository.delete(bankAccount))
                .thenReturn(true);

        Mockito.when(iBankAccountRepository.findById(id))
                .thenReturn(Optional.of(bankAccount));
        
        //when
        boolean result = bankAccountService.deleteBankAccountById(id);
        //then
        Assert.assertTrue(result);
    }
    
    @Test
    public void whenNotDeleted() {
    	
        //given
        String id = UUID.randomUUID().toString();
        BankAccount bankAccount = new BankAccount();
        bankAccount.setId(id);

        Mockito.when(iBankAccountRepository.findById(id))
                .thenReturn(Optional.empty());
        
        //when
        boolean result = bankAccountService.deleteBankAccountById(id);
        //then
        Assert.assertFalse(result);
    }
}
