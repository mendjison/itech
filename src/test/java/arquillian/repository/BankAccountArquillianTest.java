package arquillian.repository;

import kiwi.itech.model.entities.BankAccount;
import kiwi.itech.model.entities.BankAccountOwner;

//import kiwi.itech.model.repository.impl.BankAccountRepository;
import kiwi.itech.repository.IBankAccountDao;
import kiwi.itech.repository.daoImpl.BankAccountDao;
import kiwi.itech.repository.generic.JpaRepository;
import kiwi.itech.repository.generic.JpaRepositoryImpl;
import org.jboss.shrinkwrap.api.spec.WebArchive;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.junit.Assert;

import java.io.File;

import javax.ejb.EJB;

//@RunWith(Arquillian.class)
public class BankAccountArquillianTest {
	@EJB(beanName = "BankAccountRepository")
	private IBankAccountDao bankAccountRepository;

	//@Deployment(testable = true, name = "bankAccount")
	public static WebArchive createDeployment() {
		/*
		 * File archiveFile = new File("target/backendKiwiKino.war"); return
		 * ShrinkWrap.createFromZipFile(WebArchive.class, archiveFile)
		 * .addAsResource(new File("src/test/resources/META-INF/test-persistence.xml"),
		 * "META-INF/persistence.xml") .addAsManifestResource(EmptyAsset.INSTANCE,
		 * "beans.xml");
		 */

		return ShrinkWrap.create(WebArchive.class)//, "bankaccount.war") 
				.addClasses(
						BankAccountDao.class, BankAccount.class, IBankAccountDao.class,
						BankAccountOwner.class, JpaRepository.class, JpaRepositoryImpl.class)
				.addAsResource(new File("src/test/resources/META-INF/test-persistence.xml"),
								"META-INF/persistence.xml") 
				.addAsManifestResource(EmptyAsset.INSTANCE,
										"beans.xml");

	}

	//@Test
	public void test(){
		//given
		BankAccount account = new BankAccount("DE652007002400425874", "Deutsche", "Deutsche Bank",new BankAccountOwner());

		//When
		BankAccount result = bankAccountRepository.save(account);
		Assert.assertNotNull(result);
		Assert.assertTrue(true);
	}
}
