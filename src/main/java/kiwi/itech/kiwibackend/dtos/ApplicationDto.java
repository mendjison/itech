package kiwi.itech.kiwibackend.dtos;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import kiwi.itech.model.entities.Application;
import kiwi.itech.model.entities.Child;
import kiwi.itech.model.entities.Partner;
import kiwi.itech.model.entities.enums.StatusType;

public class ApplicationDto {

	private String id;
	private LocalDate applicationDate;
	private StatusType status;
	private ApplicantDto applicant;
	private List<ChildDto> childs;
	private PartnerDto partner;
	private BankAccountDto account;

	public ApplicationDto() {}

	public String getId() {
		return id;
	}

	public ApplicationDto setId(String id) {
		this.id = id;
		return this;
	}

	public LocalDate getApplicationDate() {
		return applicationDate;
	}

	public ApplicationDto setApplicationDate(LocalDate applicationDate) {
		this.applicationDate = applicationDate;
		return this;
	}

	public StatusType getStatus() {
		return status;
	}

	public ApplicationDto setStatus(StatusType status) {
		this.status = status;
		return this;
	}

	public ApplicantDto getApplicant() {
		return applicant;
	}

	public ApplicationDto setApplicant(ApplicantDto applicant) {
		this.applicant = applicant;
		return this;
	}

	public List<ChildDto> getChilds() {
		if(this.childs == null)
			this.childs = new ArrayList<ChildDto>();
		return childs;
	}

	public ApplicationDto setChilds(List<ChildDto> childs) {
		this.childs = childs;
		return this;
	}

	public BankAccountDto getAccount() {
		return account;
	}

	public ApplicationDto setAccount(BankAccountDto account) {
		this.account = account;
		return this;
	}

	public PartnerDto getPartner() {
		return partner;
	}

	public ApplicationDto setPartner(PartnerDto partner) {
		this.partner = partner;
		return this;
	}

	@Override
	public String toString() {
		return "ApplicationDto [id=" + id + ", applicationDate=" + applicationDate + ", status=" + status + "]";
	}

	public Application toPojo() {
		Application application = new Application();
		application.setAccount(getAccount().toPojo());
		application.setPartner((getPartner() == null || getPartner().isNull())? null: getPartner().toPojo());
		application.setApplicant(getApplicant().toPojo());
		application.setId(getId());
		application.setApplicationDate(getApplicationDate());
		application.setStatus(getStatus());

		List<Child> childList = new ArrayList<>();
		if(getChilds() != null && !getChilds().isEmpty()) {
			getChilds().stream()
					.forEach((ChildDto child) -> childList.add(child.toPojo()));
		}
		application.setChilds(childList);
		return  application;
	}
	
	
}
