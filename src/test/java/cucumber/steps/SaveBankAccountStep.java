package cucumber.steps;

import java.lang.reflect.Type;
import java.util.List;

import javax.ejb.EJB;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.Before;
import io.cucumber.java.DefaultDataTableCellTransformer;
import io.cucumber.java.DefaultDataTableEntryTransformer;
import io.cucumber.java.DefaultParameterTransformer;
import kiwi.itech.repository.IBankAccountDao;
import org.junit.Assert;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import kiwi.itech.model.entities.BankAccount;
import kiwi.itech.model.entities.BankAccountOwner;

public class SaveBankAccountStep {


    @EJB(beanName = "BankAccountRepository")
    private IBankAccountDao bankAccountRepository;
    private BankAccount bankAccount;


    @Given("the bank information")
    public void the_bank_information(List<BankAccount> bankAccounts) {

        bankAccount = bankAccounts.get(0);
    }

    @Given("is to owner")
    public void is_to_owner(List<BankAccountOwner> bankAccountOwners) {
        bankAccount.setAccountOwner(bankAccountOwners.get(0));
    }
    @When("call \\/bank")
    public void call_bank() {
        bankAccountRepository.save(bankAccount);
    }
    @Then("the status is ok")
    public void the_status_is_ok() {
        Assert.assertTrue(true);
    }
}
